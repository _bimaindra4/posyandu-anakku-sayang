/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 100408
 Source Host           : localhost:3306
 Source Schema         : dbanakkusayang

 Target Server Type    : MySQL
 Target Server Version : 100408
 File Encoding         : 65001

 Date: 18/11/2020 09:48:47
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for d_anak
-- ----------------------------
DROP TABLE IF EXISTS `d_anak`;
CREATE TABLE `d_anak`  (
  `id_anak` int(11) NOT NULL AUTO_INCREMENT,
  `kms_anak` bigint(11) NOT NULL,
  `nik_ibu` bigint(11) NULL DEFAULT NULL,
  `nama_anak` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `anak_tgl_lahir` date NULL DEFAULT NULL,
  `umur_anak` int(11) NULL DEFAULT NULL,
  `anak_bb_lahir` int(11) NULL DEFAULT NULL,
  `anak_tb_lahir` int(11) NULL DEFAULT NULL,
  `anak_kelamin` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `anak_tmpt_lahir` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_anak`) USING BTREE,
  INDEX `kms_anak`(`kms_anak`) USING BTREE,
  INDEX `nik_ibu`(`nik_ibu`) USING BTREE,
  CONSTRAINT `d_anak_ibfk_1` FOREIGN KEY (`nik_ibu`) REFERENCES `d_ibu` (`nik_ibu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of d_anak
-- ----------------------------
INSERT INTO `d_anak` VALUES (2, 1234, 123456789, 'Khanza', '2020-06-05', 24, 3, 53, 'Perempuan', 'Solo');
INSERT INTO `d_anak` VALUES (3, 12345, 123456789, 'Anak', '2020-11-23', NULL, 3, 50, 'Laki-Laki', 'bidan');

-- ----------------------------
-- Table structure for d_ibu
-- ----------------------------
DROP TABLE IF EXISTS `d_ibu`;
CREATE TABLE `d_ibu`  (
  `id_ibu` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NULL DEFAULT NULL,
  `nik_ibu` bigint(50) NOT NULL,
  `nama_ibu` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat_ibu` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `suami_ibu` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ibu_tgl_lahir` date NULL DEFAULT NULL,
  `ibu_tempat_lahir` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `rt` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ibu_umur` int(11) NULL DEFAULT NULL,
  `ibu_telpon` varchar(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_kehamilan` date NULL DEFAULT NULL,
  `status_ibu` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `berat_badan` int(3) NULL DEFAULT NULL,
  `penggunaan_kontrasepsi` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `jml_persalinan` int(2) NULL DEFAULT NULL,
  `jml_keguguran` int(2) NULL DEFAULT NULL,
  `jml_anak_hidup` int(2) NULL DEFAULT NULL,
  `jml_lahir_mati` int(2) NULL DEFAULT NULL,
  `jml_anak_lahir_kurang_bulan` int(2) NULL DEFAULT NULL,
  `jarak_kehamilan` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status_imun_tt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `imun_tt_terakhir` date NULL DEFAULT NULL,
  `penolong_persalinan` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `cara_persalinan` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_ibu`) USING BTREE,
  INDEX `fk_id_user`(`id_user`) USING BTREE,
  INDEX `nik_ibu`(`nik_ibu`) USING BTREE,
  CONSTRAINT `fk_id_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of d_ibu
-- ----------------------------
INSERT INTO `d_ibu` VALUES (1, 3, 123456789, 'Ibu', 'Malang', 'Suami', '1971-05-01', 'Malang', '03', 49, '41231321', '2020-03-19', '1', 60, '-', 1, 0, 0, 0, 0, '0', '-', '2020-01-01', '-', '-');
INSERT INTO `d_ibu` VALUES (2, 4, 35703035403930002, 'Diana', 'Malang', 'Ranidah', '1993-02-14', 'Malang', '12', 27, '0812345967472', '2020-06-10', '1', 71, 'tidak ada', 0, 0, 0, 0, NULL, NULL, 'normal', '2020-10-29', '-', '-');
INSERT INTO `d_ibu` VALUES (3, 5, 3507014909990005, 'Riska', 'Malang', 'Erwin', '1999-12-09', 'Malang', '04', 20, '08235938575', '2020-09-16', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `d_ibu` VALUES (4, 6, 123123123123, 'Wulan', 'Malang', 'Alek', '1992-06-05', 'Malang', '05', 28, '34732987`', NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `d_ibu` VALUES (5, 7, 56546546546, 'Ninda', 'Malang', 'Aldo', '1995-09-02', 'Malang', '07', 25, '9853345', NULL, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for d_imunisasi
-- ----------------------------
DROP TABLE IF EXISTS `d_imunisasi`;
CREATE TABLE `d_imunisasi`  (
  `id_imunisasi` int(11) NOT NULL,
  `nama_imunisasi` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jumlah` int(11) NULL DEFAULT NULL,
  `jenis` enum('anak','ibu') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_imunisasi`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of d_imunisasi
-- ----------------------------
INSERT INTO `d_imunisasi` VALUES (0, 'Imunisasi Lain', 36, NULL);
INSERT INTO `d_imunisasi` VALUES (1, 'Hepatitis B (Anak)', 39, 'anak');
INSERT INTO `d_imunisasi` VALUES (2, 'BCG', 28, 'anak');
INSERT INTO `d_imunisasi` VALUES (3, 'Polio', 29, 'anak');
INSERT INTO `d_imunisasi` VALUES (4, 'DPT', 31, 'anak');
INSERT INTO `d_imunisasi` VALUES (5, 'Campak', 29, 'anak');
INSERT INTO `d_imunisasi` VALUES (6, 'Difteri Toksoid', 10, 'ibu');
INSERT INTO `d_imunisasi` VALUES (7, 'Pneumokokus', 8, 'ibu');
INSERT INTO `d_imunisasi` VALUES (8, 'Meningokokus', 7, 'ibu');
INSERT INTO `d_imunisasi` VALUES (9, 'Hepatitis A', 7, 'ibu');
INSERT INTO `d_imunisasi` VALUES (10, 'Hepatitis B (Ibu)', 10, 'ibu');

-- ----------------------------
-- Table structure for d_kader
-- ----------------------------
DROP TABLE IF EXISTS `d_kader`;
CREATE TABLE `d_kader`  (
  `kader_id` int(11) NOT NULL,
  `id_user` int(11) NULL DEFAULT NULL,
  `kader_nama` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kader_tlp` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kader_id`) USING BTREE,
  INDEX `id_user`(`id_user`) USING BTREE,
  CONSTRAINT `d_kader_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of d_kader
-- ----------------------------
INSERT INTO `d_kader` VALUES (1, 2, 'Siti Khodijah', '08123428894');

-- ----------------------------
-- Table structure for d_lay_anak
-- ----------------------------
DROP TABLE IF EXISTS `d_lay_anak`;
CREATE TABLE `d_lay_anak`  (
  `id_lay_anak` int(11) NOT NULL AUTO_INCREMENT,
  `kms_anak` bigint(11) NULL DEFAULT NULL,
  `id_imunisasi` int(11) NULL DEFAULT NULL,
  `imunisasi_lain` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `umur_anak` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tgl_pelayanan` date NULL DEFAULT NULL,
  `bb_anak` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tb_anak` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_lay_anak`) USING BTREE,
  INDEX `id_imunisasi`(`id_imunisasi`) USING BTREE,
  INDEX `kms_anak`(`kms_anak`) USING BTREE,
  CONSTRAINT `d_lay_anak_ibfk_2` FOREIGN KEY (`id_imunisasi`) REFERENCES `d_imunisasi` (`id_imunisasi`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `d_lay_anak_ibfk_3` FOREIGN KEY (`kms_anak`) REFERENCES `d_anak` (`kms_anak`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of d_lay_anak
-- ----------------------------
INSERT INTO `d_lay_anak` VALUES (1, 1234, 0, 'Vitamin A', '24', '2020-11-08', '3', '53');

-- ----------------------------
-- Table structure for d_lay_bumil
-- ----------------------------
DROP TABLE IF EXISTS `d_lay_bumil`;
CREATE TABLE `d_lay_bumil`  (
  `id_lay_bumil` int(11) NOT NULL AUTO_INCREMENT,
  `nik_ibu` bigint(11) NULL DEFAULT NULL,
  `id_imunisasi` int(11) NULL DEFAULT NULL,
  `tgl_pelayanan` date NULL DEFAULT NULL,
  `berat_badan` int(5) NULL DEFAULT NULL,
  `keluhan` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `tekanan_darah` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `hamil_ke` int(1) NULL DEFAULT NULL,
  `umur_kehamilan` int(3) NULL DEFAULT NULL,
  `lingkar_lengan` int(3) NULL DEFAULT NULL,
  `tinggi_fundus` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `letak_janin` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `denyut_jantung_janin` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kaki_bengkak` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tempat_periksa_hamil` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_resiko_tinggi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `usia_anak_terakhir` int(10) NULL DEFAULT NULL,
  `bpjs_kis` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_htp` date NULL DEFAULT NULL,
  `tanggal_hpht` date NULL DEFAULT NULL,
  `hasil_lab` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `tindakan` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `nasihat` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `keterangan` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `kapan_kembali` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_lay_bumil`) USING BTREE,
  INDEX `id_imunisasi`(`id_imunisasi`) USING BTREE,
  INDEX `nik_ibu`(`nik_ibu`) USING BTREE,
  CONSTRAINT `d_lay_bumil_ibfk_2` FOREIGN KEY (`id_imunisasi`) REFERENCES `d_imunisasi` (`id_imunisasi`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `d_lay_bumil_ibfk_3` FOREIGN KEY (`nik_ibu`) REFERENCES `d_ibu` (`nik_ibu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of d_lay_bumil
-- ----------------------------
INSERT INTO `d_lay_bumil` VALUES (1, 123456789, 8, '2020-09-01', 60, 'dd', 'sad', 1, 8, 20, '10', 'tes', '12', '0', 'Dokter', '-', 2, 'ya', NULL, NULL, '-', '-', '-', '-', NULL);
INSERT INTO `d_lay_bumil` VALUES (2, 123456789, 7, '2020-11-08', 67, 'Pusing', '110/70', 1, 39, 23, NULL, 'perut', '90', '1', 'rumah sakit', 'tidak ada', NULL, 'ya', '2021-01-21', '2020-07-15', '-', '-', '-', '-', NULL);
INSERT INTO `d_lay_bumil` VALUES (3, 35703035403930002, 8, '2020-11-12', 71, 'Mual', '110/80', 1, 22, 25, NULL, 'posterior', '100', '1', 'dokter', 'tidak ada', NULL, 'ya', '2020-12-24', '2020-10-22', 'tidak ada', 'normal', 'tidak ada', 'sehat', NULL);
INSERT INTO `d_lay_bumil` VALUES (4, 3507014909990005, 7, '2020-11-12', 70, 'Pusing', '110/80', 1, 8, 26, NULL, 'tes', '105', '1', 'bidan', 'tidak ada', NULL, 'ya', '2021-05-13', '2020-10-10', 'tidak ada', 'tidak ada', 'banyak minum airr putih', 'tidak ada', NULL);

-- ----------------------------
-- Table structure for d_lay_pus_wus
-- ----------------------------
DROP TABLE IF EXISTS `d_lay_pus_wus`;
CREATE TABLE `d_lay_pus_wus`  (
  `id_lay_puswus` int(11) NOT NULL AUTO_INCREMENT,
  `nik_ibu` bigint(11) NULL DEFAULT NULL,
  `id_imunisasi` int(11) NULL DEFAULT NULL,
  `tgl_pelayanan` date NULL DEFAULT NULL,
  `usia_anak_terakhir` int(2) NULL DEFAULT NULL,
  `kb_jenis` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kb_lama` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `tgl_imunisasi_tt` date NULL DEFAULT NULL,
  PRIMARY KEY (`id_lay_puswus`) USING BTREE,
  INDEX `id_imunisasi`(`id_imunisasi`) USING BTREE,
  INDEX `nik_ibu`(`nik_ibu`) USING BTREE,
  CONSTRAINT `d_lay_pus_wus_ibfk_2` FOREIGN KEY (`id_imunisasi`) REFERENCES `d_imunisasi` (`id_imunisasi`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `d_lay_pus_wus_ibfk_3` FOREIGN KEY (`nik_ibu`) REFERENCES `d_ibu` (`nik_ibu`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of d_lay_pus_wus
-- ----------------------------
INSERT INTO `d_lay_pus_wus` VALUES (2, 123123123123, NULL, '2020-11-18', 2, 'tablet', 'tablet', '-', '2020-10-16');
INSERT INTO `d_lay_pus_wus` VALUES (3, 56546546546, NULL, '2020-11-18', 2, 'tablet', 'pil', '-', '2020-11-13');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `role` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', '12345', 'admin');
INSERT INTO `user` VALUES (2, 'siti', '1234567890', 'kader');
INSERT INTO `user` VALUES (3, '123456789', '123456789', 'ibu');
INSERT INTO `user` VALUES (4, '35703035403930002', '35703035403930002', 'ibu');
INSERT INTO `user` VALUES (5, '3507014909990005', '3507014909990005', 'ibu');
INSERT INTO `user` VALUES (6, '123123123123', '123123123123', 'ibu');
INSERT INTO `user` VALUES (7, '56546546546', '56546546546', 'ibu');

SET FOREIGN_KEY_CHECKS = 1;
