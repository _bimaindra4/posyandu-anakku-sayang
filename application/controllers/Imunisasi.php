<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Imunisasi extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Model_Imunisasi');
		$this->load->model('Model_app');

		if ($this->session->userdata('status') != "login") {
			redirect(base_url("welcome"));
		}
		$this->role = $this->session->userdata('role');
		$this->nama_kader = $this->session->userdata('nama_kader');
	}

	public function Index()
	{
		if ($this->role == "kader"){
		$data['header'] = $this->load->view('template/header-kader', $this->nama_kader, TRUE);
		$data['footer'] = $this->load->view('template/footer-kader', $this->nama_kader, TRUE);
		$data['imunisasi'] = $this->Model_Imunisasi->GetDataImunisasi();

		$this->load->view('imunisasi/tabel_imunisasi', $data);
		} else if ($this->role == "admin"){
			$data['header'] = $this->load->view('template/header-admin', 'Admin', TRUE);
			$data['footer'] = $this->load->view('template/footer-admin', 'Admin', TRUE);
			$data['imunisasi'] = $this->Model_Imunisasi->GetDataImunisasi();

			$this->load->view('admin/imunisasi', $data);
		}
	}

	public function Detail($id)
	{
		$data['header'] = $this->load->view('template/header-kader', $this->nama_kader, TRUE);
		$data['footer'] = $this->load->view('template/footer-kader', $this->nama_kader, TRUE);
		$data['anak'] = $this->ModelAnak->GetDataAnak($id)->row();

		$this->load->view('imunisasi/detail_imunisasi', $data);
	}

	public function TambahStok()
	{
		$id = $this->input->post("jenis");
		$tambahStok = $this->input->post("stok");

		$dataImun = $this->Model_Imunisasi->GetDataImunisasi($id);
		$stokImun = $dataImun->row()->jumlah;

		$data = [
			"jumlah" => $tambahStok + $stokImun
		];

		$res = $this->Model_Imunisasi->TambahStokImunisasi($data, $id);
		if ($res == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Penambahan Stok Berhasil.');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan.');
		}
		redirect("imunisasi");
	}

	public function Transaksi()
	{
		$data['header'] = $this->load->view('template/header-kader', $this->nama_kader, TRUE);
		$data['footer'] = $this->load->view('template/footer-kader', $this->nama_kader, TRUE);
		$data['imunisasi'] = $this->Model_Imunisasi->GetDataImunisasiAnak();

		$this->load->view('imunisasi/transaksi', $data);
	}

	public function TambahAnakImunisasi()
	{
		$id_imunisasi = $this->input->post("imunisasi");
		$data = [
			"id_lay_anak" => $this->Model_app->getLatestid('id_lay_anak', 'd_lay_anak'),
			"kms_anak" => $this->input->post("kms_anak"),
			"id_imunisasi" => $id_imunisasi,
			"imunisasi_lain" => $this->input->post("imunisasi_lain"),
			"umur_anak" => $this->input->post("umur_anak"),
			"tgl_pelayanan" => date("Y-m-d"),
			"bb_anak" => $this->input->post("bb_anak"),
			"tb_anak" => $this->input->post("tb_anak")
		];

		$res = $this->Model_Imunisasi->TambahAnakImunisasi($data, $id_imunisasi);
		if ($res == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Imunisasi anak berhasil.');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan.');
		}
		redirect("anak");
	}

	public function TambahImunisasi(){
		$data = [
			"id_imunisasi" => $this->Model_app->getLatestid('id_imunisasi', 'd_imunisasi'),
			"nama_imunisasi" => $this->input->post('imunisasi'),
			"jumlah" => $this->input->post('stok'),
			"jenis" => $this->input->post('jenis')
		];

		$res = $this->Model_Imunisasi->TambahDataImunisasi($data);
		if ($res == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Penambahan Imunisasi berhasil.');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan.');
		}
		redirect("imunisasi");
	}

	public function find_anak_by_kms_nama($attr, $val)
	{
		$output = "";
		$res = $this->Model_Imunisasi->FindAnakByKmsNama($attr, $val);

		if ($res->num_rows() != 0) {
			if ($attr == "kms") {
				$anak = $res->row();
				$output .= '
				<div class="card mt-2">
                    <div class="p-3">
                        <div class="float-right">
                            <a href="#" class="btn btn-primary btn-sm mt-3" onclick="get_data_anak(' . $anak->kms_anak . ')">Pilih</a>
                        </div>
                        <div class="image mr-3  float-left">
                            <img class="user_avatar no-b no-p r-5" src="' . base_url() . 'assets/img/baby.png" alt="User Image">
                        </div>
                        <div>
                            <h6 class="p-t-10">' . $anak->nama_anak . '</h6>
                            ' . $anak->kms_anak . '
                        </div>
                    </div>
                </div>
				';
			} else if ($attr = "nama") {
				foreach ($res->result() as $anak) {
					$output .= '
					<div class="card mt-2">
						<div class="p-3">
							<div class="float-right">
								<a href="#" class="btn btn-primary btn-sm mt-3" onclick="get_data_anak(' . $anak->kms_anak . ')">Pilih</a>
							</div>
							<div class="image mr-3  float-left">
								<img class="user_avatar no-b no-p r-5" src="' . base_url() . 'assets/img/baby.png" alt="User Image">
							</div>
							<div>
								<h6 class="p-t-10">' . $anak->nama_anak . '</h6>
								' . $anak->kms_anak . '
							</div>
						</div>
					</div>
					';
				}
			}
		}

		$data = ["output" => $output];
		echo json_encode($data);
	}

	public function get_data_anak($id)
	{
		$res = $this->Model_Imunisasi->FindAnakByKmsNama("kms", $id);
		$anak = $res->row();
		$data['kms_anak'] = $anak->kms_anak;
		$data['anak'] = '
		<div class="card">
			<div class="p-3">
				<div class="image mr-3  float-left">
					<img class="user_avatar no-b no-p r-5" src="' . base_url() . 'assets/img/baby.png" alt="User Image">
				</div>
				<div>
					<h6 class="p-t-10">' . $anak->nama_anak . '</h6>
					' . $anak->kms_anak . '
				</div>
			</div>
		</div>
		';

		// Get Data Imunisasi
		$res_imun = $this->Model_Imunisasi->GetImunisasiAnak($id);
		if ($res_imun->num_rows() == 0) {
			$data['imunisasi'] = '<tr><td colspan="3" class="text-center">Data Kosong</td></tr>';
		} else {
			$output_imun = '';
			$no = 1;
			foreach ($res_imun->result() as $imun) {
				$output_imun .= '
				<tr>
					<td>' . $no . '</td>
					<td>' . $imun->nama_imunisasi . '</td>
					<td>' . date("d/m/Y", strtotime($imun->tgl_pelayanan)) . '</td>
				</tr>
				';
				$no++;
			}
			$data['imunisasi'] = $output_imun;
		}

		echo json_encode($data);
	}
}
