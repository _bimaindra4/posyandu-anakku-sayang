<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kader extends CI_Controller
{
	protected $role;
	protected $iduser;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Model_Anak');
		$this->load->model('Model_Ibu');
		$this->load->model('Model_app');
		$this->load->model('Model_Imunisasi');
		$this->load->model('Model_Kader');
		$this->load->model('Model_login');

		if ($this->session->userdata('status') != "login") {
			redirect(base_url("welcome"));
		}
		$this->role = $this->session->userdata('role');
		$this->iduser = $this->session->userdata('id_user');
		$this->username = $this->session->userdata('nama');

	}

	public function index()
	{
		$data['header'] = $this->load->view('template/header-admin', $this->username, TRUE);
		$data['footer'] = $this->load->view('template/footer-admin', $this->username, TRUE);
		$data['kader'] = $this->Model_Kader->getDataKader()->result();

		$this->load->view('admin/kader', $data);
	}

	public function ibu(){
		$data['header'] = $this->load->view('template/header-admin', 'Admin', TRUE);
		$data['footer'] = $this->load->view('template/footer-admin', 'Admin', TRUE);
		$data['ibu'] = $this->Model_Ibu->GetDataIbu();
		$this->load->view('admin/ibu', $data);
	}

	public function anak(){
		$data['header'] = $this->load->view('template/header-admin', 'Admin', TRUE);
		$data['footer'] = $this->load->view('template/footer-admin', 'Admin', TRUE);
		$data['anak'] = $this->Model_Anak->GetDataAnak();

		$this->load->view('admin/anak', $data);
	}

	public function TambahKader()
	{
		$id_user = $this->Model_app->getLatestid('id_user', 'user');
		$data = [
			"kader_id" => $this->Model_app->getLatestid('kader_id', 'd_kader'),
			"id_user" => $id_user,
			"kader_nama" => $this->input->post("nama_kader"),
			"kader_tlp" => $this->input->post("telepon"),
		];

		$data_user = [
			"id_user" => $id_user,
			"username" => $this->input->post("username"),
			"password" => $this->input->post("password"),
			"role" => "kader"
		];

		$this->Model_login->AddUserKader($data_user);
		$res = $this->Model_Kader->TambahKader($data);
		if ($res) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Penambahan Data Kader Berhasil.');
			redirect('kader');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan.');
		}
	}

	public function EditKader($id)
	{
		$id_user = $this->Model_Kader->GetIdUserfromIdKader($id);
		$data = [
			"kader_nama" => $this->input->post("nama_kader"),
			"kader_tlp" => $this->input->post("tlp_kader"),
		];

		$datauser = [
			"username" => $this->input->post("user_kader"),
			"password" => $this->input->post("pass_kader"),
		];

		$res = $this->Model_Kader->EditKader($data, $id);
		if ($res == TRUE) {
			$user = $this->Model_Kader->EditUserKader($datauser, $id_user);
			if ($user == TRUE) {
				$this->session->set_flashdata('stts', TRUE);
				$this->session->set_flashdata('message', 'Edit Data Kader Berhasil.');
			} else {
				$this->session->set_flashdata('message', 'Terjadi kesalahan.');
			}
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan.');
		}
		redirect('kader');
	}

	public function HapusKader($id)
	{
		$proc = $this->Model_Kader->HapusKader($id);
		if ($proc == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Berhasil menghapus data');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan');
		}
		redirect('Kader');
	}

	public function get_detail_kader($id)
	{
		$sql = $this->Model_Kader->getDataKader($id);
		echo json_encode($sql->row());
	}

}
