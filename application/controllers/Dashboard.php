<?php
class Dashboard extends CI_Controller {
	protected $role;
	protected $iduser;

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('status') != "login") {
			redirect(base_url("login"));
		}
		
		$this->load->model("Model_Ibu");
		$this->load->model("Model_Anak");
		$this->load->model("Model_Imunisasi");
		$this->load->model("Model_Kader");

		$this->role = $this->session->userdata('role');
		$this->iduser = $this->session->userdata('id_user');
		$this->nama_ibu = $this->session->userdata('nama_ibu');
		$this->nama_kader = $this->session->userdata('nama_kader');
	}

	public function index() {
		if($this->role == "kader") {
			$data['header'] = $this->load->view('template/header-kader', $this->nama_kader, TRUE);
			$data['footer'] = $this->load->view('template/footer-kader', $this->nama_kader, TRUE);
			$data['ibu'] = $this->Model_Ibu->GetDataIbu()->num_rows();
			$data['anak'] = $this->Model_Anak->GetDataAnak()->num_rows();
			$data['imunisasi'] = $this->Model_Imunisasi->GetDataImunisasi()->num_rows();
	
			$this->load->view('index', $data);
		} else if($this->role == "ibu") {
			$data['header'] = $this->load->view('template/header-ibu', $this->nama_ibu, TRUE);
			$data['footer'] = $this->load->view('template/footer-ibu', $this->nama_ibu, TRUE);
			$data['ibu'] = $this->Model_Ibu->GetDetailIbu($this->iduser);

			$this->load->view('role_ibu/index', $data);
		} else if($this->role == "admin"){
			$data['header'] = $this->load->view('template/header-admin', 'Admin', TRUE);
			$data['footer'] = $this->load->view('template/footer-admin', 'Admin', TRUE);
			$data['kader'] = $this->Model_Kader->getDataKader();

			$this->load->view('admin/kader', $data);
		}
	}
}
