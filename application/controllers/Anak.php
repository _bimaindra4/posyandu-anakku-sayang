<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Anak extends CI_Controller
{
	protected $role;
	protected $iduser;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Model_Anak');
		$this->load->model('Model_Ibu');
		$this->load->model('Model_app');
		$this->load->model('Model_Imunisasi');

		if ($this->session->userdata('status') != "login") {
			redirect(base_url("welcome"));
		}
		$this->role = $this->session->userdata('role');
		$this->iduser = $this->session->userdata('id_user');
		$this->nama_ibu = $this->session->userdata('nama_ibu');
		$this->nama_kader = $this->session->userdata('nama_kader');
	}

	public function Index()
	{
		if ($this->role == "kader") {
			$data['header'] = $this->load->view('template/header-kader', $this->nama_kader, TRUE);
			$data['footer'] = $this->load->view('template/footer-kader', $this->nama_kader, TRUE);
			$data['anak'] = $this->Model_Anak->GetDataAnak();
			$data['ibu'] = $this->Model_Ibu->GetNamaIbu()->result();
			$data['imunisasi'] = $this->Model_Imunisasi->GetDataImunisasiAnak();

			$this->load->view('anak/tabel_anak', $data);
		} else if ($this->role == "ibu") {
			$data['header'] = $this->load->view('template/header-ibu', $this->nama_ibu, TRUE);
			$data['footer'] = $this->load->view('template/footer-ibu', $this->nama_ibu, TRUE);
			$data['anak'] = $this->Model_Anak->GetDataAnakByNIKIbu($this->iduser);

			$this->load->view('role_ibu/tabel_anak', $data);
		}
	}

	public function Detail($id)
	{
		if ($this->role == "kader") {
			$data['header'] = $this->load->view('template/header-kader', $this->nama_kader, TRUE);
			$data['footer'] = $this->load->view('template/footer-kader', $this->nama_kader, TRUE);
			$data['anak'] = $this->Model_Anak->GetDataAnak($id)->row();
			$data['histori'] = $this->Model_Anak->DetailHistoriImunisasi($id);
			$data['bb_anak'] = $this->Model_Anak->GetBBAnak($id);

			$this->load->view('anak/detail_anak', $data);
		} else if ($this->role == "ibu") {
			$data['header'] = $this->load->view('template/header-ibu', $this->nama_ibu, TRUE);
			$data['footer'] = $this->load->view('template/footer-ibu', $this->nama_ibu, TRUE);
			$data['anak'] = $this->Model_Anak->GetDataAnak($id)->row();
			$data['histori'] = $this->Model_Anak->DetailHistoriImunisasi($id);
			$data['bb_anak'] = $this->Model_Anak->GetBBAnak($id);

			$this->load->view('role_ibu/detail_anak', $data);
		}
	}

	public function TambahAnak()
	{
		date_default_timezone_set('Asia/Jakarta');
		$data = [
			"id_anak" => $this->Model_app->getLatestid('id_anak', 'd_anak'),
			"kms_anak" => $this->input->post("kms_anak"),
			"nama_anak" => $this->input->post("nama_anak"),
			"nik_ibu" => $this->input->post("nama_ibu"),
			"anak_tgl_lahir" => $this->input->post("tgl_lahir"),
			"anak_bb_lahir" => $this->input->post("bb_lahir"),
			"umur_anak" => $this->input->post("umur_anak"),
			"anak_tb_lahir" => $this->input->post("tb_lahir"),
			"anak_kelamin" => $this->input->post("jkel"),
			"anak_tmpt_lahir" => $this->input->post("tempat_lahir")
		];

		$res = $this->Model_Anak->TambahAnak($data);
		if ($res == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Penambahan Data Anak Berhasil.');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan.');
		}
		redirect('Anak');
	}

	public function LahiranAnak()
	{
		date_default_timezone_set('Asia/Jakarta');
		$nik_ibu = $this->Model_app->getDataByParameter("id_ibu", $this->input->post("id_ibu"), "d_ibu")->row()->nik_ibu;
		$data = [
			"id_anak" => $this->Model_app->getLatestid('id_anak', 'd_anak'),
			"kms_anak" => $this->input->post("kms_anak"),
			"nama_anak" => $this->input->post("nama_anak"),
			"nik_ibu" => $nik_ibu,
			"anak_tgl_lahir" => $this->input->post("tgl_lahir"),
			"anak_bb_lahir" => $this->input->post("bb_lahir"),
//			"umur_anak" => $this->input->post("umur_anak"),
			"anak_tb_lahir" => $this->input->post("tb_lahir"),
			"anak_kelamin" => $this->input->post("jkel"),
			"anak_tmpt_lahir" => $this->input->post("tempat_lahir")
		];

		$dataibu = [
			"status_ibu" => 3,
		];

		$res = $this->Model_Anak->TambahAnak($data);
		if ($res == TRUE) {
			$ibu = $this->Model_Ibu->EditIbu($dataibu, $this->input->post("id_ibu"));
			if ($ibu == TRUE) {
				$this->session->set_flashdata('stts', TRUE);
				$this->session->set_flashdata('message', 'Penambahan Data Anak Berhasil.');
			} else {
				$this->session->set_flashdata('message', 'Terjadi kesalahan.');
			}
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan.');
		}
		redirect('Anak');
	}

	public function EditAnak($id)
	{
		date_default_timezone_set('Asia/Jakarta');

		$data = [
			"kms_anak" => $this->input->post("kms_anak"),
			"nama_anak" => $this->input->post("nama_anak"),
			"anak_tgl_lahir" => date("Y-m-d", strtotime($this->input->post("tgl_lahir"))),
			"anak_bb_lahir" => $this->input->post("bb_lahir"),
			"anak_kelamin" => $this->input->post("jkel"),
			"anak_tmpt_lahir" => $this->input->post("tempat_lahir")
		];

		$res = $this->Model_Anak->EditAnak($data, $id);
		if ($res == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Perubahan Data Anak Berhasil.');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan.');
		}
		redirect("anak");
	}

	public function HapusAnak($id)
	{
		$res = $this->Model_Anak->HapusAnak($id);
		if ($res == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Penghapusan Data Anak Berhasil.');
		} else {
			$this->session->set_flashdata('message', 'Terjadi Kesalahan.');
		}
		redirect("anak");
	}

		public function get_detail_anak($id)
		{
			$sql = $this->Model_Anak->GetDataAnak($id);
			echo json_encode($sql->row());
		}

	public function get_layanan_anak($anak, $id)
	{
		$sql = $this->Model_Anak->GetLayananAnak($anak, $id);
		echo json_encode($sql->row());
	}
}
