<?php

class Login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Model_login');
	}

	function index()
	{
		$data['err_message'] = "";
		$this->load->view('login', $data);
	}

	function aksi_login()
	{
		$username = $this->input->post('username');
		$pass = $this->input->post('password');

		$auth_log = $this->Model_login->cek_login($username, $pass);
		$cek = $auth_log->num_rows();
		if ($cek > 0) {
			$row_log = $auth_log->row();
			$role = $row_log->role;
			$id_user = $row_log->id_user;
			$nama_ibu = $row_log->nama_ibu;
			$nama_kader = $row_log->kader_nama;
			session_start();
			$data_session = array(
				'nama' => $username,
				'status' => "login",
				'role' => $role,
				'id_user' => $id_user,
				'nama_ibu' => $nama_ibu,
				'nama_kader' => $nama_kader,
			);
			$_SESSION['username'] = $username;
			$this->session->set_userdata($data_session);

			if ($role == "admin"){
				redirect("kader");
			} else {
				redirect("dashboard");
			}
		} else {
			$data['err_message'] = "Username dan password salah !";
			$this->load->view('login', $data);
		}
	}
}

