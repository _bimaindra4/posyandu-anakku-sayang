<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ibu extends CI_Controller
{
	protected $role;
	protected $iduser;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Model_Ibu');
		$this->load->model('Model_login');
		$this->load->model('Model_app');
		$this->load->model("Model_Imunisasi");
		$this->load->model("Model_Anak");

		if ($this->session->userdata('status') != "login") {
			redirect(base_url("Welcome"));
		}
		$this->role = $this->session->userdata('role');
		$this->iduser = $this->session->userdata('id_user');
		$this->nama_ibu = $this->session->userdata('nama_ibu');
		$this->nama_kader = $this->session->userdata('nama_kader');
	}

	public function index()
	{
		$data['header'] = $this->load->view('template/header-kader', $this->nama_kader, TRUE);
		$data['footer'] = $this->load->view('template/footer-kader', $this->nama_kader, TRUE);
		$data['ibu'] = $this->Model_Ibu->GetDataIbu();
		$this->load->view('ibu/tabel_ibu', $data);
	}

	public function pus_wus()
	{
		if ($this->role == "kader") {
			$data['header'] = $this->load->view('template/header-kader', $this->nama_kader, TRUE);
			$data['footer'] = $this->load->view('template/footer-kader', $this->nama_kader, TRUE);
			$data['puswus'] = $this->Model_Ibu->GetDataIbuPUSWUS();
			$this->load->view('ibu/tabel_ibu_puswus', $data);
		} else if ($this->role == "ibu") {
			$data['header'] = $this->load->view('template/header-ibu', $this->nama_ibu, TRUE);
			$data['footer'] = $this->load->view('template/footer-ibu', $this->nama_ibu, TRUE);
			$data['konsultasi'] = $this->Model_Ibu->GetKonsultasiIbuPUSWUS($this->iduser);

			$this->load->view('role_ibu/pus_wus', $data);
		}
	}

	public function hamil()
	{
		if ($this->role == "kader") {
			$data['header'] = $this->load->view('template/header-kader', $this->nama_kader, TRUE);
			$data['footer'] = $this->load->view('template/footer-kader', $this->nama_kader, TRUE);
			$data['hamil'] = $this->Model_Ibu->GetDataIbuHamil();
			$this->load->view('ibu/tabel_ibu_hamil', $data);
		} else if ($this->role == "ibu") {
			$data['header'] = $this->load->view('template/header-ibu', $this->nama_ibu, TRUE);
			$data['footer'] = $this->load->view('template/footer-ibu', $this->nama_ibu, TRUE);
			$data['ibu'] = $this->Model_Ibu->GetDetailIbu($this->iduser);
			$data['konsultasi'] = $this->Model_Ibu->GetKonsultasiIbu($this->iduser);

			$this->load->view('role_ibu/hamil', $data);
		}
	}

	public function detail_puswus($id)
	{
		$data['header'] = $this->load->view('template/header-kader', $this->nama_kader, TRUE);
		$data['footer'] = $this->load->view('template/footer-kader', $this->nama_kader, TRUE);
		$data['ibu'] = $this->Model_Ibu->GetDetailIbu($id);
		$data['imunisasi'] = $this->Model_Imunisasi->GetDataImunisasiIbu();
		$data['konsultasi'] = $this->Model_Ibu->GetKonsultasiIbuPUSWUS($id);

		$this->load->view('ibu/detail_ibu_puswus', $data);
	}

	public function detail_hamil($id)
	{
		$data['header'] = $this->load->view('template/header-kader', $this->nama_kader, TRUE);
		$data['footer'] = $this->load->view('template/footer-kader', $this->nama_kader, TRUE);
		$data['ibu'] = $this->Model_Ibu->GetDetailIbu($id);
		$data['imunisasi'] = $this->Model_Imunisasi->GetDataImunisasiIbu();
		$data['konsultasi'] = $this->Model_Ibu->GetKonsultasiIbu($id);

		$this->load->view('ibu/detail_ibu_hamil', $data);
	}

	public function edit_detail_ibu($id)
	{
		$data['header'] = $this->load->view('template/header-kader', $this->nama_kader, TRUE);
		$data['footer'] = $this->load->view('template/footer-kader', $this->nama_kader, TRUE);
		$data['ibu'] = $this->Model_Ibu->GetDetailIbu($id);
		$this->load->view('ibu/edit_detail_ibu', $data);
	}

	public function TambahIbu()
	{
		$id_user = $this->Model_app->getLatestid('id_user', 'user');
		$status_ibu = $this->input->post("status");
		$nik_ibu = $this->input->post("nik_ibu");
		$tgl_lahir = $this->input->post("tgl_lahir");
		$dob = new DateTime($tgl_lahir);
		$now = new DateTime();
		$difference = $now->diff($dob);
		$age = $difference->y;
		$data = [
			"id_ibu" => $this->Model_app->getLatestid('id_ibu', 'd_ibu'),
			"nik_ibu" => $nik_ibu,
			"id_user" => $id_user,
			"nama_ibu" => $this->input->post("nama_ibu"),
			"suami_ibu" => $this->input->post("nama_suami"),
			"ibu_tgl_lahir" => $tgl_lahir,
//			"umur_kehamilan" => $this->input->post("umur_kehamilan"),
			"rt" => $this->input->post("rt"),
			"tanggal_kehamilan" => $this->input->post("tanggal_kehamilan"),
			"ibu_tempat_lahir" => $this->input->post("tempat_lahir"),
			"ibu_umur" => $age,
			"alamat_ibu" => $this->input->post("tempat_lahir"),
			"ibu_telpon" => $this->input->post("nomor_tlp"),
			"status_ibu" => $status_ibu
		];

		$data_user = [
			"id_user" => $id_user,
			"username" => $nik_ibu,
			"password" => $nik_ibu,
			"role" => "ibu"
		];

		if ($this->Model_app->is_nik_available($nik_ibu)) {
			$this->session->set_flashdata('message', 'NIK Sudah Terdaftar.');
			redirect('ibu');
		} else {
			$this->Model_login->AddUserIbu($data_user);
			$res = $this->Model_Ibu->TambahIbu($data);
			if (isset($res)) {
				if ($status_ibu == 1) {
					$this->session->set_flashdata('stts', TRUE);
					$this->session->set_flashdata('message', 'Penambahan Data Ibu Berhasil.');
					redirect('ibu/detail_hamil/' . $res);
				} else {
					$this->session->set_flashdata('stts', TRUE);
					$this->session->set_flashdata('message', 'Penambahan Data Ibu Berhasil.');
					redirect('ibu/detail_puswus/' . $res);
				}
			} else {
				$this->session->set_flashdata('message', 'Terjadi kesalahan.');
			}
		}
	}

	public function EditIbu($id)
	{
		$data = [
			"nama_ibu" => $this->input->post("nama_ibu"),
			"suami_ibu" => $this->input->post("nama_suami"),
			"ibu_tgl_lahir" => $this->input->post("tgl_lahir"),
			"ibu_tempat_lahir" => $this->input->post("tempat_lahir"),
			"alamat_ibu" => $this->input->post("alamat"),
			"ibu_telpon" => $this->input->post("nomor_tlp"),
			"status_ibu" => $this->input->post("status"),
			"tanggal_kehamilan" => $this->input->post("tanggal_kehamilan")
		];

		$res = $this->Model_Ibu->EditIbu($data, $id);
		if ($res == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Edit Data Ibu Berhasil.');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan.');
		}
		redirect('ibu');
	}

	public function EditIbuHamil($id)
	{
		$data = [
			"nama_ibu" => $this->input->post("nama_ibu"),
			"suami_ibu" => $this->input->post("nama_suami"),
			"alamat_ibu" => $this->input->post("alamat"),
			"ibu_telpon" => $this->input->post("nomor_tlp")
		];

		$res = $this->Model_Ibu->EditIbu($data, $id);
		if ($res == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Edit Data Ibu Berhasil.');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan.');
		}
		redirect('ibu/hamil');
	}

	public function EditNikIbu($id)
	{
		$id_user = $this->Model_Ibu->GetIdUserfromIdIbu($id);
		$nikibu = $this->input->post("nik_ibu");
		$data = [
			"nik_ibu" => $nikibu,
		];

		$datauser = [
			"username" => $nikibu,
			"password" => $nikibu
		];

		if ($this->Model_app->is_nik_available($nikibu)) {
			$this->session->set_flashdata('message', 'NIK Sudah Terdaftar.');
			redirect('ibu');
		} else {
			$res = $this->Model_Ibu->EditIbu($data, $id);
			if ($res == TRUE) {
				$user = $this->Model_Ibu->EditUserIbu($datauser, $id_user);
				if ($user == TRUE) {
					$this->session->set_flashdata('stts', TRUE);
					$this->session->set_flashdata('message', 'Edit Data Ibu Berhasil.');
				} else {
					$this->session->set_flashdata('message', 'Terjadi kesalahan.');
				}
			} else {
				$this->session->set_flashdata('message', 'Terjadi kesalahan.');
			}
			redirect('ibu');
		}
	}

	public function EditDetailIbu($id)
	{
		$data = [
//			"lingkar_lengan" => $this->input->post("lingkar_lengan"),
			"ibu_umur" => $this->input->post("umur"),
			"ibu_tgl_lahir" => date("Y-m-d", strtotime($this->input->post("tgl_lahir"))),
			"berat_badan" => $this->input->post("berat_badan"),
			"penggunaan_kontrasepsi" => $this->input->post("kontrasepsi"),
			"jml_persalinan" => $this->input->post("persalinan"),
			"jml_keguguran" => $this->input->post("keguguran"),
			"jml_anak_hidup" => $this->input->post("anak_hidup"),
			"jml_lahir_mati" => $this->input->post("lahir_mati"),
//			"jml_anak_lahir_kurang_bulan" => $this->input->post("kurang_bulan"),
//			"jarak_kehamilan" => $this->input->post("persalinan_terakhir"),
			"status_imun_tt" => $this->input->post("stts_imunisasi"),
			"imun_tt_terakhir" => date("Y-m-d", strtotime($this->input->post("imunisasi_terakhir"))),
			"penolong_persalinan" => $this->input->post("penolong_salin"),
			"cara_persalinan" => $this->input->post("cara_salin")
		];

		$res = $this->Model_Ibu->EditIbu($data, $id);
		if ($res == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Edit Data Ibu Berhasil.');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan.');
		}
		redirect('ibu/detail_hamil/' . $id);
	}

	public function HapusIbu($id)
	{
		$proc = $this->Model_Ibu->HapusIbu($id);
		if ($proc == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Berhasil menghapus data');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan');
		}
		redirect('Ibu');
	}

	public function TambahLayananIbu($id)
	{
		$nikIbu = $this->db->select("nik_ibu")->where("id_ibu", $id)->get("d_ibu")->row()->nik_ibu;
		$id_imunisasi = $this->input->post("imunisasi");
		$data = [
			"id_lay_bumil" => $this->Model_app->getLatestid('id_lay_bumil', 'd_lay_bumil'),
			"nik_ibu" => $nikIbu,
			"id_imunisasi" => $id_imunisasi,
			"tgl_pelayanan" => date("Y-m-d"),
			"berat_badan" => $this->input->post("bb"),
			"keluhan" => $this->input->post("keluhan"),
			"tekanan_darah" => $this->input->post("tek_darah"),
			"hamil_ke" => $this->input->post("hamil_ke"),
			"umur_kehamilan" => $this->input->post("umur"),
			"lingkar_lengan" => $this->input->post("lingkar_lengan"),
//			"tinggi_fundus" => $this->input->post("tinggi_fundus"),
			"letak_janin" => $this->input->post("letak_janin"),
			"denyut_jantung_janin" => $this->input->post("detak_jantung"),
			"kaki_bengkak" => $this->input->post("kaki_bengkak"),
			"tempat_periksa_hamil" => $this->input->post("tempat_periksa"),
			"jenis_resiko_tinggi" => $this->input->post("resiko"),
//			"usia_anak_terakhir" => $this->input->post("usia_anak"),
			"bpjs_kis" => $this->input->post("bpjs_kis"),
			"tanggal_hpht" => date("Y-m-d", strtotime($this->input->post("hpht"))),
			"tanggal_htp" => date("Y-m-d", strtotime($this->input->post("htp"))),
			"hasil_lab" => $this->input->post("hasil_lab"),
			"tindakan" => $this->input->post("tindakan"),
			"nasihat" => $this->input->post("nasihat"),
			"keterangan" => $this->input->post("keterangan")
		];

		$proc = $this->Model_Ibu->TambahLayananIbu($data, $id_imunisasi);
		if ($proc == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Berhasil menambahkan data');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan');
		}
		redirect('ibu/detail_hamil/' . $id);
	}

	public function TambahLayananIbuPUSWUS($id)
	{
		$nikIbu = $this->db->select("nik_ibu")->where("id_ibu", $id)->get("d_ibu")->row()->nik_ibu;
//		$id_imunisasi = $this->input->post("imunisasi");
		$data = [
			"id_lay_puswus" => $this->Model_app->getLatestid('id_lay_puswus', 'd_lay_pus_wus'),
			"nik_ibu" => $nikIbu,
//			"id_imunisasi" => $id_imunisasi,
			"tgl_pelayanan" => date("Y-m-d"),
			"tgl_imunisasi_tt" => $this->input->post("imuntt"),
			"usia_anak_terakhir" => $this->input->post("usia_anak"),
			"kb_jenis" => $this->input->post("kb_jenis"),
			"kb_lama" => $this->input->post("kb_lama"),
			"keterangan" => $this->input->post("keterangan")
		];

		$proc = $this->Model_Ibu->TambahLayananIbuPUSWUS($data);
		if ($proc == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Berhasil menambahkan data');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan');
		}
		redirect('ibu/detail_puswus/' . $id);
	}

	public function EditLayananIbu($id)
	{
		$nikIbu = $this->db->select("nik_ibu")->where("id_lay_bumil", $id)->get("d_lay_bumil")->row()->nik_ibu;
		$idIbu = $this->db->select("id_ibu")->where("nik_ibu", $nikIbu)->get("d_ibu")->row()->id_ibu;

		$data = [
			"id_imunisasi" => $this->input->post("imunisasi"),
			"berat_badan" => $this->input->post("bb"),
			"keluhan" => $this->input->post("keluhan"),
			"tekanan_darah" => $this->input->post("tek_darah"),
//			"hamil_ke" => $this->input->post("hamil_ke"),
//			"umur_kehamilan" => $this->input->post("umur"),
			"lingkar_lengan" => $this->input->post("lingkar_lengan"),
//			"tinggi_fundus" => $this->input->post("tinggi_fundus"),
			"letak_janin" => $this->input->post("letak_janin"),
			"denyut_jantung_janin" => $this->input->post("detak_jantung"),
			"kaki_bengkak" => $this->input->post("kaki_bengkak"),
			"tempat_periksa_hamil" => $this->input->post("tempat_periksa"),
			"jenis_resiko_tinggi" => $this->input->post("resiko"),
//			"usia_anak_terakhir" => $this->input->post("usia_anak"),
			"bpjs_kis" => $this->input->post("bpjs_kis"),
			"tanggal_hpht" => date("Y-m-d", strtotime($this->input->post("hpht"))),
			"tanggal_htp" => date("Y-m-d", strtotime($this->input->post("htp"))),
			"hasil_lab" => $this->input->post("hasil_lab"),
			"tindakan" => $this->input->post("tindakan"),
			"nasihat" => $this->input->post("nasihat"),
			"keterangan" => $this->input->post("keterangan")
		];

		$proc = $this->Model_Ibu->EditLayananIbu($data, $id);
		if ($proc == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Berhasil mengedit layanan');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan');
		}
		redirect('ibu/detail_hamil/' . $idIbu);
	}

	public function EditLayananIbuPUSWUS($id)
	{
		$nikIbu = $this->db->select("nik_ibu")->where("id_lay_puswus", $id)->get("d_lay_pus_wus")->row()->nik_ibu;
		$idIbu = $this->db->select("id_ibu")->where("nik_ibu", $nikIbu)->get("d_ibu")->row()->id_ibu;

		$data = [
//			"id_imunisasi" => $this->input->post("imunisasi"),
			"tgl_imunisasi_tt" => $this->input->post("imuntt"),
			"tgl_pelayanan" => date("Y-m-d"),
			"usia_anak_terakhir" => $this->input->post("usia_anak"),
			"kb_jenis" => $this->input->post("kb_jenis"),
			"kb_lama" => $this->input->post("kb_lama"),
			"keterangan" => $this->input->post("keterangan")
		];

		$proc = $this->Model_Ibu->EditLayananIbuPUSWUS($data, $id);
		if ($proc == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Berhasil mengedit layanan');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan');
		}
		redirect('ibu/detail_puswus/' . $idIbu);
	}

	public function HapusLayananIbu($id)
	{
		$nikIbu = $this->db->select("nik_ibu")->where("id_lay_bumil", $id)->get("d_lay_bumil")->row()->nik_ibu;
		$idIbu = $this->db->select("id_ibu")->where("nik_ibu", $nikIbu)->get("d_ibu")->row()->id_ibu;

		$proc = $this->Model_Ibu->HapusLayananIbu($id);
		if ($proc == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Berhasil menghapus layanan');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan');
		}
		redirect('ibu/detail/' . $idIbu);
	}

	public function HapusLayananIbuPUSWUS($id)
	{
		$nikIbu = $this->db->select("nik_ibu")->where("id_lay_puswus", $id)->get("d_lay_pus_wus")->row()->nik_ibu;
		$idIbu = $this->db->select("id_ibu")->where("nik_ibu", $nikIbu)->get("d_ibu")->row()->id_ibu;

		$proc = $this->Model_Ibu->HapusLayananIbuPUSWUS($id);
		if ($proc == TRUE) {
			$this->session->set_flashdata('stts', TRUE);
			$this->session->set_flashdata('message', 'Berhasil menghapus layanan');
		} else {
			$this->session->set_flashdata('message', 'Terjadi kesalahan');
		}
		redirect('ibu/detail_puswus/' . $idIbu);
	}

	public function add()
	{
		$this->load->view('template/header-kader');
		$this->load->view('ibu/form_ibuhamil');
	}

	public function detail_ibu($kms)
	{
		$sql = $this->Model_Ibu->GetDetailIbu($kms);
		echo json_encode($sql);
	}

	public function get_layanan_ibu($ibu, $id)
	{
		$sql = $this->Model_Ibu->GetKonsultasiIbu($ibu, $id);
		echo json_encode($sql->row());
	}

	public function get_layanan_ibu_puswus($ibu, $id)
	{
		$sql = $this->Model_Ibu->GetKonsultasiIbuPUSWUS($ibu, $id);
		echo json_encode($sql->row());
	}

	function check_nik_avalibility()
	{
		if ($this->Model_app->is_nik_available($this->input->post('nik_ibu'))) {
			echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> NIK Sudah Terdaftar</label>';
		} else {
			echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span> NIK Belum Terdaftar</label>';
		}
	}

	function laporan_ibu()
	{
		$date = date("Ymd");
		$month = $this->input->post("bulan");
		$year = $this->input->post("tahun");

		switch ($month) {
			case 1:
				$month_text = "Januari";
				break;
			case 2:
				$month_text = "Februari";
				break;
			case 3:
				$month_text = "Maret";
				break;
			case 4:
				$month_text = "April";
				break;
			case 5:
				$month_text = "Mei";
				break;
			case 6:
				$month_text = "Juni";
				break;
			case 7:
				$month_text = "Juli";
				break;
			case 8:
				$month_text = "Agustus";
				break;
			case 9:
				$month_text = "September";
				break;
			case 10:
				$month_text = "Oktober";
				break;
			case 11:
				$month_text = "November";
				break;
			case 12:
				$month_text = "Desember";
				break;
			default:
				$month_text = "";
		}

		$data["month_text"] = $month_text;
		$data["year"] = $year;
		$data["title"] = "Laporan Ibu Hamil Bulan " . $month_text . " Tahun " . $year;
		$data['laporan'] = $this->Model_Ibu->GetLaporanIbuPerBulan($month, $year);
//		$data['laporan_anak'] = $this->Model_Ibu->GetLaporanAnakPerBulan($month, $year);

		$this->load->library('pdf');

		$this->pdf->setPaper('A4', 'landscape');
		$this->pdf->filename = "laporan_pendataan_" . $date . ".pdf";
		$this->pdf->load_view('laporan_pdf_ibu', $data);
//		$this->load->view("laporan_pdf_ibu", $data);
	}

	function laporan_pus_wus() {
		$date = date("Ymd");
		$month = $this->input->post("bulan");
		$year = $this->input->post("tahun");

		switch ($month) {
			case 1:
				$month_text = "Januari";
				break;
			case 2:
				$month_text = "Februari";
				break;
			case 3:
				$month_text = "Maret";
				break;
			case 4:
				$month_text = "April";
				break;
			case 5:
				$month_text = "Mei";
				break;
			case 6:
				$month_text = "Juni";
				break;
			case 7:
				$month_text = "Juli";
				break;
			case 8:
				$month_text = "Agustus";
				break;
			case 9:
				$month_text = "September";
				break;
			case 10:
				$month_text = "Oktober";
				break;
			case 11:
				$month_text = "November";
				break;
			case 12:
				$month_text = "Desember";
				break;
			default:
				$month_text = "";
		}

		$data["month_text"] = $month_text;
		$data["year"] = $year;
		$data["title"] = "Laporan Ibu PUS/WUS Bulan " . $month_text . " Tahun " . $year;
		$laporan = $this->Model_Ibu->GetLaporanIbuPUSWUSBulan($month, $year);
		$data_puswus = [];
		
		if(count($laporan) > 0) {
			foreach($laporan as $row) {
				$output = $row;
				$output->anak_l = $this->Model_Anak->GetTotalDataAnakByNIKIbuFilterJK($row->id_ibu, "Laki-Laki");
				$output->anak_p = $this->Model_Anak->GetTotalDataAnakByNIKIbuFilterJK($row->id_ibu, "Perempuan");

				$data_puswus[] = $output;
			}
		}
		$data['laporan'] = $data_puswus;

		$this->load->library('pdf');

		$this->pdf->setPaper('A4', 'landscape');
		$this->pdf->filename = "laporan_pendataan_puswus_".$date.".pdf";
		$this->pdf->load_view('laporan_pdf_pus_wus', $data);
	}
}
