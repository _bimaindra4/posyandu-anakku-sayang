<?php

class Model_Anak extends CI_Model {
    public function __construct() {
        $this->load->database();
    }

    public function GetDataAnak($id = "") {
        if($id != "") {
            $this->db->select("da.*");
            $this->db->where("id_anak", $id);
        } else {
            $this->db->select("da.*, di.nama_ibu");
            $this->db->join("d_ibu di", "da.nik_ibu = di.nik_ibu", "left");
        }

		$sql = $this->db->get("d_anak da");
		return $sql;
    }

    public function GetDataAnakByNIKIbu($id = NULL) {
        $this->db->select("anak.*");
        $this->db->join("d_ibu ibu", "anak.nik_ibu = ibu.nik_ibu");
        $this->db->where("ibu.id_ibu", $id);
        $sql = $this->db->get("d_anak anak");
        return $sql;
    }

    public function GetTotalDataAnakByNIKIbuFilterJK($id, $jk) {
        $this->db->select("anak.*");
        $this->db->join("d_ibu ibu", "anak.nik_ibu = ibu.nik_ibu");
        $this->db->where("ibu.id_ibu", $id);
        $this->db->where("anak.anak_kelamin", $jk);
        $sql = $this->db->get("d_anak anak");
        return $sql->num_rows();
    }

    public function GetBBAnak($id) {
        $this->db->select("lay.umur_anak, lay.bb_anak");
        $this->db->join("d_anak anak", "lay.kms_anak = anak.kms_anak");
        $this->db->where("anak.id_anak", $id);
        $this->db->order_by("lay.umur_anak", "DESC");
        $sql = $this->db->get("d_lay_anak lay");

        return $sql->result();
    }

    public function TambahAnak($data) {
        $sql = $this->db->insert("d_anak", $data);
        return $sql;
    }

    public function EditAnak($data, $id) {
        $sql = $this->db->update("d_anak", $data, ["id_anak" => $id]);
        return $sql;
    }

    public function HapusAnak($id) {
        $sql = $this->db->delete("d_anak", ["id_anak" => $id]);
        return $sql;
    }

	public function DetailHistoriImunisasi($id){
		$this->db->select("dla.*, IF(dla.id_imunisasi = 0, dla.imunisasi_lain, di.nama_imunisasi) as nama_imunisasi");
		$this->db->from("d_lay_anak AS dla");
		$this->db->join("d_anak AS da", "dla.kms_anak = da.kms_anak");
		$this->db->join("d_imunisasi AS di", "dla.id_imunisasi = di.id_imunisasi");
		$this->db->where("da.id_anak", $id);
		$this->db->order_by("dla.umur_anak", "ASC");
		$sql = $this->db->get();
		return $sql->result_array();
    }

		public function GetLayananAnak($id){
			$this->db->select("lay.*, imun.nama_imunisasi AS imunisasi");
			$this->db->where("lay.id_lay_anak", $id);
			$this->db->join("d_imunisasi imun", "lay.id_imunisasi = imun.id_imunisasi", "left");
			$sql = $this->db->get("d_lay_anak lay");
			return $sql;
		}
}

?>
