<?php

class Model_Ibu extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

	public function GetDataIbu() {
		$this->db->select("di.*, user.*");
		$this->db->join("user", "di.id_user = user.id_user");
		$sql = $this->db->get("d_ibu di");
		return $sql;
	}

	public function GetNamaIbu() {
		$this->db->select("*");
		$sql = $this->db->get("d_ibu");
		return $sql;
	}

	public function GetIdUserfromIdIbu($id){
		$this->db->select("u.id_user");
		$this->db->from("user u");
		$this->db->join("d_ibu di", "u.id_user = di.id_user");
		$this->db->where("di.id_ibu", $id);
		$sql = $this->db->get();
		return $sql->row()->id_user;
	}

	public function GetIdIbufromIdUser($id){
		$this->db->select("di.id_ibu");
		$this->db->from("d_ibu di");
		$this->db->join("user u", "di.id_user = u.id_user");
		$this->db->where("u.id_user", $id);
		$sql = $this->db->get();
		return $sql->row()->id_ibu;
	}

	public function GetDetailIbu($id){
		$this->db->select("ibu.*, bumil.*");
		$this->db->join("d_lay_bumil bumil", "ibu.nik_ibu = bumil.nik_ibu", "left");
		$this->db->where("id_ibu", $id);
		$sql = $this->db->get("d_ibu ibu");
		return $sql->row();
	}

	public function GetDataIbuHamil() {
		$this->db->select("di.*, user.*");
		$this->db->join("user", "di.id_user = user.id_user");
		$this->db->where("status_ibu", 1);
		$sql = $this->db->get("d_ibu di");
		return $sql;
	}

	public function GetDataIbuPUSWUS() {
		$this->db->select("*");
		$this->db->where("status_ibu", 2);
		$sql = $this->db->get("d_ibu");
		return $sql;
	}

	public function GetKonsultasiIbu($ibu, $id = "") {
		if($id != "") {
			$this->db->select("lay.*, imun.nama_imunisasi AS imunisasi, ibu.tanggal_kehamilan");
			$this->db->where("lay.id_lay_bumil", $id);
		} else {
			$this->db->select("lay.id_lay_bumil, imun.nama_imunisasi AS imunisasi, 
							   lay.tgl_pelayanan, lay.keluhan, ibu.tanggal_kehamilan");
		}
		$this->db->join("d_imunisasi imun", "lay.id_imunisasi = imun.id_imunisasi", "left");
		$this->db->join("d_ibu ibu", "lay.nik_ibu = ibu.nik_ibu");
		$this->db->where("ibu.id_ibu", $ibu);
		$sql = $this->db->get("d_lay_bumil lay");
		return $sql;
	}

	public function GetKonsultasiIbuPUSWUS($ibu,$id = "") {
		if($id != "") {
			$this->db->select("lay.*, imun.nama_imunisasi AS imunisasi");
			$this->db->where("lay.id_lay_puswus", $id);
		} else {
			$this->db->select("lay.*, imun.nama_imunisasi AS imunisasi");
		}
		$this->db->join("d_imunisasi imun", "lay.id_imunisasi = imun.id_imunisasi", "left");
		$this->db->join("d_ibu ibu", "lay.nik_ibu = ibu.nik_ibu");
		$this->db->where("ibu.id_ibu", $ibu);
		$sql = $this->db->get("d_lay_pus_wus lay");
		return $sql;
	}

	public function GetLaporanIbuPerBulan($month,$year) {
		$this->db->select('
			ibu.nama_ibu, ibu.suami_ibu, ibu.ibu_umur,
			ibu.alamat_ibu, ibu.rt, ibu.nik_ibu,
			lay.hamil_ke, lay.umur_kehamilan,
			IFNULL(lay.tanggal_hpht,"-") AS hpht, lay.lingkar_lengan AS lila,
			lay.tempat_periksa_hamil, lay.jenis_resiko_tinggi,
			lay.usia_anak_terakhir, ibu.ibu_telpon, lay.bpjs_kis
		');
		$this->db->join("d_lay_bumil lay", "ibu.nik_ibu = lay.nik_ibu");
		$this->db->where("YEAR(lay.tgl_pelayanan)", $year);
		$this->db->where("MONTH(lay.tgl_pelayanan)", $month);
		$sql = $this->db->get("d_ibu ibu");
		
		return $sql->result();
	}

	public function GetLaporanIbuPUSWUSBulan($month,$year) {
		$this->db->select('
			ibu.id_ibu, ibu.nik_ibu, ibu.nama_ibu, ibu.suami_ibu,
			ibu.ibu_tgl_lahir, ibu.ibu_umur AS usia,
			pw.usia_anak_terakhir, pw.kb_jenis, pw.kb_lama, "" AS kode,
			pw.keterangan
		');
		$this->db->join("d_ibu ibu", "pw.nik_ibu = ibu.nik_ibu");
		$this->db->where("YEAR(pw.tgl_pelayanan)", $year);
		$this->db->where("MONTH(pw.tgl_pelayanan)", $month);
		$sql = $this->db->get("d_lay_pus_wus pw");

		return $sql->result();
	}

	public function GetLaporanAnakPerBulan($month,$year) {
		$this->db->select('
			ibu.nama_ibu, ibu.suami_ibu, ibu.ibu_umur,
			ibu.alamat_ibu, "01" AS rt, ibu.nik_ibu, lay_ibu.bpjs_kis,
			anak.nama_anak, IF (anak.anak_kelamin = "Laki-Laki", "L", "P") as kelamin, anak.anak_tgl_lahir, anak.anak_tmpt_lahir,
			anak.anak_bb_lahir, anak.anak_tb_lahir
		');
		$this->db->join("d_ibu ibu", "anak.nik_ibu = ibu.nik_ibu");
		$this->db->join("d_lay_bumil lay_ibu", "ibu.nik_ibu = lay_ibu.nik_ibu");
		$this->db->where("YEAR(anak.anak_tgl_lahir)", $year);
		$this->db->where("MONTH(anak.anak_tgl_lahir)", $month);
		$sql = $this->db->get("d_anak anak");

		return $sql->result();
	}

	public function TambahIbu($data) {
		$this->db->insert("d_ibu", $data);
		return $this->db->insert_id();
	}

	public function TambahLayananIbu($data, $id_imunisasi) {
		$sql = $this->db->insert("d_lay_bumil", $data);

		$this->db->select("jumlah");
		$this->db->where("id_imunisasi", $id_imunisasi);
		$imun = $this->db->get("d_imunisasi")->row();

		$this->db->update("d_imunisasi", ["jumlah" => ($imun->jumlah - 1)], ["id_imunisasi" => $id_imunisasi]);
		return $sql;
	}

	public function TambahLayananIbuPUSWUS($data) {
		return $this->db->insert("d_lay_pus_wus", $data);

//		$this->db->select("jumlah");
//		$this->db->where("id_imunisasi", $id_imunisasi);
//		$imun = $this->db->get("d_imunisasi")->row();
//
//		$this->db->update("d_imunisasi", ["jumlah" => ($imun->jumlah - 1)], ["id_imunisasi" => $id_imunisasi]);
//		return $sql;
	}

	public function EditIbu($data, $id) {
		$sql = $this->db->update("d_ibu", $data, ["id_ibu" => $id]);
		return $sql;
	}

	public function EditUserIbu($data, $id){
		$sql = $this->db->update("user", $data, ["id_user" => $id]);
		return $sql;
	}

	public function EditLayananIbu($data, $id) {
		$sql = $this->db->update("d_lay_bumil", $data, ["id_lay_bumil" => $id]);
		return $sql;
	}

	public function EditLayananIbuPUSWUS($data, $id) {
		$sql = $this->db->update("d_lay_pus_wus", $data, ["id_lay_puswus" => $id]);
		return $sql;
	}

	public function HapusIbu($id) {
		$sql = $this->db->delete("user", ["id_user" => $id]);
		return $sql;
	}

	public function HapusLayananIbu($id) {
		$sql = $this->db->delete("d_lay_bumil", ["id_lay_bumil" => $id]);
		return $sql;
	}

	public function HapusLayananIbuPUSWUS($id) {
		$sql = $this->db->delete("d_lay_pus_wus", ["id_lay_puswus" => $id]);
		return $sql;
	}
}

?>
