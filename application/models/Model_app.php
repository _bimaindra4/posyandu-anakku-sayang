<?php


class Model_app extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	function getLatestid($key, $table) {
		$this->db->select_max($key);
		$query = $this->db->get($table);
		$result = $query->row_array();

		return $result[$key] + 1;
	}

	function is_nik_available($nik){
		$this->db->where('nik_ibu', $nik);
		$query = $this->db->get("d_ibu");
		return $query->num_rows() > 0;
	}

	function getDataByParameter($parameters, $values, $tables){
		return $this->db->where($parameters, $values)->get($tables);
	}

}
