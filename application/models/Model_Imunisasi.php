<?php

class Model_Imunisasi extends CI_Model {

	public function __construct() {

	}

	public function GetDataImunisasi($id = "") {
		if($id != "") {
			$this->db->select("*");
			$this->db->where("id_imunisasi", $id);
		} else {
			$this->db->select("*");
			$this->db->where("id_imunisasi >", 0);
		}

		$sql = $this->db->get("d_imunisasi");
		return $sql;
	}

	public function GetDataImunisasiAnak() {
		$this->db->select("*");
		$this->db->where("jumlah >", 0);
		$this->db->where("jenis", "anak");
		$sql = $this->db->get("d_imunisasi");
		return $sql;
	}

	public function GetDataImunisasiIbu() {
		$this->db->select("*");
		$this->db->where("jumlah >", 0);
		$this->db->where("jenis", "ibu");
		$sql = $this->db->get("d_imunisasi");
		return $sql;
	}
	
	public function GetImunisasiAnak($id) {
		$this->db->select("IF(dla.id_imunisasi = 0, dla.imunisasi_lain, di.nama_imunisasi) as nama_imunisasi, dla.tgl_pelayanan");
		$this->db->join("d_imunisasi di", "dla.id_imunisasi = di.id_imunisasi");
		$this->db->where("dla.kms_anak", $id);
		$sql = $this->db->get("d_lay_anak dla");
		return $sql;
	}

	public function TambahStokImunisasi($data,$id) {
		$sql = $this->db->update("d_imunisasi", $data, ["id_imunisasi" => $id]);
		return $sql;
	}

	public function TambahDataImunisasi($data){
		return $this->db->insert("d_imunisasi", $data);
	}

	public function TambahAnakImunisasi($data, $id_imunisasi) {
		$sql = $this->db->insert("d_lay_anak", $data);

		$this->db->select("jumlah");
		$this->db->where("id_imunisasi", $id_imunisasi);
		$imun = $this->db->get("d_imunisasi")->row();

		$this->db->update("d_imunisasi", ["jumlah" => ($imun->jumlah - 1)], ["id_imunisasi" => $id_imunisasi]);
		return $sql;
	}

	public function FindAnakByKmsNama($attr,$val) {
		$this->db->select("id_anak, kms_anak, nama_anak");
		
		if($attr == "kms") {
			$this->db->where("kms_anak", $val);
			$sql = $this->db->get("d_anak");
		} else if($attr == "nama") {
			$this->db->like("nama_anak", $val);
			$sql = $this->db->get("d_anak");
		}

		return $sql;
	}
}

?>
