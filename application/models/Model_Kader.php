<?php

class Model_Kader extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function getDataKader($id = "")
	{
		if ($id != "") {
			$this->db->select("kader.*, user.*");
			$this->db->join("user", "kader.id_user = user.id_user");
			$this->db->where("kader_id", $id);
		} else {
			$this->db->select("kader.*");
		}

		$sql = $this->db->get("d_kader kader");
		return $sql;
	}

	public function TambahKader($data)
	{
		return $this->db->insert("d_kader", $data);
	}

	public function EditKader($data, $id)
	{
		$sql = $this->db->update("d_kader", $data, ["kader_id" => $id]);
		return $sql;
	}

	public function EditUserKader($data, $id)
	{
		$sql = $this->db->update("user", $data, ["id_user" => $id]);
		return $sql;
	}

	public function HapusKader($id)
	{
		$sql = $this->db->delete("user", ["id_user" => $id]);
		return $sql;
	}

	public function GetIdUserfromIdKader($id)
	{
		$this->db->select("u.id_user");
		$this->db->from("user u");
		$this->db->join("d_kader k", "u.id_user = k.id_user");
		$this->db->where("k.kader_id", $id);
		$sql = $this->db->get();
		return $sql->row()->id_user;
	}

}

?>
