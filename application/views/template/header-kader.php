<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="<?php echo base_url() ?>assets/img/posyandu.ico" type="image/x-icon">
	<title>Posyandu Si Anak Sayang</title>
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/app.css">
	<style>
		.loader {
			position: fixed;
			left: 0;
			top: 0;
			width: 100%;
			height: 100%;
			background-color: #F5F8FA;
			z-index: 9998;
			text-align: center;
		}

		.plane-container {
			position: absolute;
			top: 50%;
			left: 50%;
		}
	</style>
	<script>(function (w, d, u) {
			w.readyQ = [];
			w.bindReadyQ = [];

			function p(x, y) {
				if (x == "ready") {
					w.bindReadyQ.push(y);
				} else {
					w.readyQ.push(x);
				}
			};var a = {ready: p, bind: p};
			w.$ = w.jQuery = function (f) {
				if (f === d || f === u) {
					return a
				} else {
					p(f)
				}
			}
		})(window, document)</script>
</head>
<body style="background-color: #eee">
<div id="app">
	<aside class="main-sidebar fixed offcanvas shadow" data-toggle='offcanvas'>
		<section class="sidebar">
			<div class="w-80px mt-3 mb-3 ml-3">
				<img src="<?php echo base_url() ?>assets/img/posyandu.png" alt="">
			</div>
			<div class="relative">
				<div class="user-panel p-3 light mb-2">
					<div>
						<div class="float-left image">
							<img class="user_avatar" src="<?php echo base_url() ?>assets/img/dummy/u2.png" alt="User Image">
						</div>
						<div class="float-left info">
							<h6 class="font-weight-light mt-2 mb-1"><?= $this->nama_kader ?></h6>
							<b>Kader Posyandu</b>
						</div>
					</div>
				</div>
			</div>
			<ul class="sidebar-menu">
				<li class="header"><strong>MENU KADER</strong></li>
				<li>
					<a href="<?php echo site_url('dashboard') ?>">
						<i class="icon icon-home2 blue-text s-18"></i>
						<span>Dashboard</span>
					</a>
				</li>
				<li>
					<a href="<?php echo site_url('imunisasi') ?>">
						<i class="icon icon-eyedropper blue-text s-18"></i>
						<span>Data Imunisasi</span>
					</a>
				</li>
				<li class="treeview">
					<a href="#">
						<i class="icon icon-child_friendly blue-text s-18"></i>
						<span>Data Ibu</span>
						<i class="icon icon-angle-left s-18 pull-right"></i>
					</a>
					<ul class="treeview-menu">
						<li>
							<a href="<?php echo site_url('ibu') ?>">
								<i class="icon icon-circle-o blue-text"></i>
								<span>Data Ibu Master</span>
							</a>
						</li>
						<li>
							<a href="<?php echo site_url('ibu/hamil') ?>">
								<i class="icon icon-circle-o blue-text"></i>
								<span>Data Ibu Hamil</span>
							</a>
						</li>
						<li>
							<a href="<?php echo site_url('ibu/pus_wus') ?>">
								<i class="icon icon-circle-o blue-text"></i>
								<span>Data Ibu PUS/WUS</span>
							</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="<?php echo site_url('anak') ?>">
						<i class="icon icon-child_care blue-text s-18"></i>
						<span>Data Anak</span>
					</a>
				</li>
			</ul>
		</section>
	</aside>
	</aside>
	<!--Sidebar End-->
	<div class="has-sidebar-left">
		<div class="sticky">
			<div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue lighten-1">
				<div class="relative">
					<a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
						<i></i>
					</a>
				</div>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li>
							<a class="btn btn-outline-primary btn-sm" href="<?php echo site_url('logout') ?>" style="color: white"><i class="icon-exit_to_app"></i> Logout</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
