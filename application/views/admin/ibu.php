<?php echo $header ?>
<div class="page has-sidebar-left">
	<?php if ($this->session->flashdata('message')){ ?>
		<div class="alert alert-dismissible <?= $this->session->flashdata('stts') == true ? 'alert-success bg-success' : 'alert-danger bg-danger' ?> text-white border-0 fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<?= $this->session->flashdata('message') ?>
		</div>
	<?php } ?>
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-child_friendly"></i> Data Ibu
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-lg-12">
				<div class="card ">
					<div class="card-header white">
						<i class="icon-child_friendly blue-text"></i>
						<strong> Data Ibu </strong>
					</div>
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover data-tables" data-options='{ "paging": false; "searching":false}'>
							<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">NIK</th>
								<th class="text-center">Nama Ibu</th>
								<th class="text-center">Nama Suami</th>
								<th class="text-center">Tempat, Tanggal Lahir</th>
								<th class="text-center">Status</th>
							</tr>
							</thead>
							<tbody>
							<?php $no=1; foreach($ibu->result() as $row) { ?>
								<tr>
									<td class="text-center"><?php echo $no ?></td>
									<td><?php echo $row->nik_ibu ?></td>
									<td><?php echo $row->nama_ibu ?></td>
									<td><?php echo $row->suami_ibu ?></td>
									<td><?php echo $row->ibu_tempat_lahir.", ".date("d/m/Y", strtotime($row->ibu_tgl_lahir)) ?></td>
									<td><?php if ($row->status_ibu == 1) {
											echo "Hamil";
										} else if ($row->status_ibu == 2){
											echo "PUS/WUS";
										} else if ($row->status_ibu == 3){
											echo "Melahirkan";
										}	?>
									</td>
								</tr>
								<?php $no++; } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $footer ?>
