<?php echo $header ?>

<div class="page has-sidebar-left">
	<?php if ($this->session->flashdata('message')) { ?>
		<div
			class="alert alert-dismissible <?= $this->session->flashdata('stts') == true ? 'alert-success bg-success' : 'alert-danger bg-danger' ?> text-white border-0 fade show"
			role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<?= $this->session->flashdata('message') ?>
		</div>
	<?php } ?>
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-child_care"></i> Data Anak
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-lg-12">
				<div class="card ">
					<div class="card-header white">
						<i class="icon-clipboard-edit blue-text"></i>
						<strong> Data Anak </strong>
					</div>
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover data-tables"
									 data-options='{ "paging": false; "searching":false}'>
							<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">KMS</th>
								<th class="text-center">Nama</th>
								<th class="text-center">Nama Ibu</th>
								<th class="text-center">Tempat, Tanggal Lahir</th>
								<th class="text-center">Umur</th>
								<th class="text-center">Berat Badan Lahir</th>
							</tr>
							</thead>
							<tbody>
							<?php $no = 1;
							foreach ($anak->result() as $row) { ?>
								<tr>
									<td><?php echo $no ?></td>
									<td><?php echo $row->kms_anak ?></td>
									<td><?php echo $row->nama_anak ?></td>
									<td><?php echo $row->nama_ibu ?></td>
									<td>
										<?php
										$tempat = $row->anak_tmpt_lahir;
										$tgl = $row->anak_tgl_lahir;
										if ($tempat == NULL && $tgl != NULL) {
											echo date("d/m/Y", strtotime($tgl));
										} else if ($tempat != NULL && $tgl == NULL) {
											echo $tempat;
										} else if ($tempat != NULL && $tgl != NULL) {
											echo $tempat . ", " . date("d/m/Y", strtotime($tgl));
										} else {
											echo '<span class="badge r-3 badge-danger">Tidak ada data</span>';
										}
										?>
									</td>
									<td><?php echo calWeek($row->anak_tgl_lahir) . " Minggu" ?></td>
									<td>
										<?php
										$bb = $row->anak_bb_lahir;
										if ($bb == NULL) {
											echo '<span class="badge r-3 badge-danger">Tidak ada data</span>';
										} else {
											echo $bb . " Kg";
										}
										?>
									</td>
								</tr>
								<?php $no++;
							} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="tambahAnak" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?php echo site_url('anak/TambahAnak') ?>" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Tambah Anak</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>KMS Anak</label>
							<input type="text" name="kms_anak" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nama Anak</label>
							<input type="text" name="nama_anak" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nama Ibu</label>
							<select class="form-control" name="nama_ibu">
								<option value="">-- Ibu --</option>
								<?php foreach ($ibu as $rowg) { ?>
									<option value="<?php echo $rowg->nik_ibu ?>"><?php echo $rowg->nama_ibu ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Tanggal Lahir</label>
							<input type="date" name="tgl_lahir" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Tempat Lahir</label>
							<input type="text" name="tempat_lahir" class="form-control" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12 mb-3">
									<label>Berat Badan Lahir (Kg)</label>
									<input type="number" step="0.1" min="0" name="bb_lahir" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12 mb-3">
									<label>Tinggi Badan Lahir (Cm)</label>
									<input type="number" step="0.1" min="0" name="tb_lahir" class="form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Jenis Kelamin</label>
							<select name="jkel" class="form-control">
								<option value="Laki-Laki">Laki-Laki</option>
								<option value="Perempuan">Perempuan</option>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="editAnak" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Anak</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="post" id="form_edit_anak">
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>KMS Anak</label>
							<input type="text" name="kms_anak" id="kms_anak_ed" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nama Anak</label>
							<input type="text" name="nama_anak" id="nama_anak_ed" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Tanggal Lahir</label>
							<input type="date" name="tgl_lahir" id="tgl_lahir_ed" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Tempat Lahir</label>
							<input type="text" name="tempat_lahir" id="tempat_lahir_ed" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Berat Badan Lahir (Kg)</label>
							<input type="number" step="0.1" min="0" max="5" name="bb_lahir" id="bb_lahir_ed" class="form-control"
										 required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Jenis Kelamin</label>
							<select name="jkel" id="jkel_ed" class="form-control">
								<option value="Laki-Laki">Laki-Laki</option>
								<option value="Perempuan">Perempuan</option>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="hapusAnak" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Anak</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="post" id="form_hapus_anak">
				<div class="modal-body">
					<p>Apakah anda yakin akan menghapus data ini?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-danger btn-sm">Hapus</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="tambahAnakImunisasi" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?php echo site_url('imunisasi/TambahAnakImunisasi') ?>" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Tambah Imunisasi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>KMS Anak</label>
							<input type="text" name="kms_anak" id="kms_anak" class="form-control" readonly>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Umur Anak (Minggu)</label>
							<input type="number" id="umur" name="umur_anak" class="form-control" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12 mb-3">
									<label>Berat Badan (Kg)</label>
									<input type="number" id="bb" step="0.1" min="0" name="bb_anak" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12 mb-3">
									<label>Tinggi Badan (cm)</label>
									<input type="number" id="tb" step="1" min="0" name="tb_anak" class="form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Pilih Imunisasi</label>
							<select name="imunisasi" class="form-control" id="imunisasi">
								<option value="">-- Pilih Imunisasi --</option>
								<?php foreach ($imunisasi->result() as $row) { ?>
									<option value="<?php echo $row->id_imunisasi ?>">
										<?php echo $row->nama_imunisasi . " (stok " . $row->jumlah . ")" ?>
									</option>
								<?php } ?>
								<option value="0">-Lainnya-</option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Imunisasi Lain</label>
							<input type="text" name="imunisasi_lain" class="form-control" id="imunisasilain" disabled="disabled">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php echo $footer ?>

<script>

	function tambah_imunisasi(id) {
		let umur = $("#tambah-imunisasi").data("umur");
		$.ajax({
			url: "<?php echo site_url('Anak/get_detail_anak/') ?>" + id,
			dataType: "JSON",
			success: function (data) {
				$("#kms_anak").val(data.kms_anak);
				$("#bb").val(data.anak_bb_lahir);
				$("#tb").val(data.anak_tb_lahir);
				$("#umur").val(umur);
				let btnTambah = $("#tambahAnakImunisasi");
				btnTambah.modal("show");
			},

			error: function (jqXHR, textStatus, errorThrown) {
				alert('Gagal mengambil data');
			}
		});
	}

	function editAnak(id) {
		$.ajax({
			url: "<?php echo site_url('Anak/get_detail_anak/') ?>" + id,
			dataType: "JSON",
			success: function (data) {
				$("#editAnak").modal("show");
				$("#form_edit_anak").attr("action", "<?php echo site_url('anak/EditAnak/') ?>" + id);
				$("#kms_anak_ed").val(data.kms_anak);
				$("#nama_anak_ed").val(data.nama_anak);
				$("#tgl_lahir_ed").val(data.anak_tgl_lahir);
				$("#tempat_lahir_ed").val(data.anak_tmpt_lahir);
				$("#bb_lahir_ed").val(data.anak_bb_lahir);
				$("#jkel_ed").val(data.anak_kelamin);
			},

			error: function (jqXHR, textStatus, errorThrown) {
				alert('Gagal mengambil data');
			}
		});
	}

	$(function () {
		$("#imunisasi").change(function () {
			if ($(this).val() == 0) {
				$("#imunisasilain").removeAttr("disabled");
				$("#imunisasilain").focus();
			} else {
				$("#imunisasilain").attr("disabled", "disabled");
			}
		});
	});

	function hapusAnak(id) {
		$("#hapusAnak").modal("show");
		$("#form_hapus_anak").attr("action", "<?php echo site_url('anak/HapusAnak/') ?>" + id);
	}
</script>
