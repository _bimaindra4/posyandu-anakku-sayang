<?php echo $header ?>

<div class="page has-sidebar-left">
	<?php if ($this->session->flashdata('message')) { ?>
		<div
			class="alert alert-dismissible <?= $this->session->flashdata('stts') == true ? 'alert-success bg-success' : 'alert-danger bg-danger' ?> text-white border-0 fade show"
			role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<?= $this->session->flashdata('message') ?>
		</div>
	<?php } ?>
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-child_care"></i> Data Kader
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-lg-12">
				<button class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#exampleModal">Tambah</button>
				<div class="card ">
					<div class="card-header white">
						<i class="icon-clipboard-edit blue-text"></i>
						<strong> Data Kader </strong>
					</div>
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover data-tables"
									 data-options='{ "paging": false; "searching":false}'>
							<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">Nama</th>
								<th class="text-center">Telepon</th>
								<th class="text-center">Aksi</th>
							</tr>
							</thead>
							<tbody>
							<?php $no = 1;
							foreach ($kader as $row) { ?>
								<tr>
									<td><?php echo $no ?></td>
									<td><?php echo $row->kader_nama ?></td>
									<td><?php echo $row->kader_tlp ?></td>
									<td class="text-center">
										<button onclick="editKader('<?php echo $row->kader_id ?>')" class="btn btn-warning btn-xs">Edit
										</button>
										<button onclick="hapusKader('<?php echo $row->id_user ?>')" class="btn btn-danger btn-xs">Hapus
										</button>
									</td>
								</tr>
								<?php $no++;
							} ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?php echo site_url('kader/TambahKader') ?>" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Tambah Kader</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nama Kader</label>
							<input type="text" name="nama_kader" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Username</label>
							<input type="text" name="username" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Password</label>
							<input type="password" name="password" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nomor Telepon</label>
							<input type="number" min="0" name="telepon" class="form-control">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="editKader" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Edit Kader</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="post" id="form_edit_kader">
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nama Kader</label>
							<input type="text" name="nama_kader" id="namakader" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Username</label>
							<input type="text" name="user_kader" id="userkader" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Password</label>
							<input type="password" name="pass_kader" id="passkader" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nomor Telepon</label>
							<input type="number" min="0" name="tlp_kader" id="tlpkader" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<input type="checkbox" onclick="myFunction()">Lihat Password
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="hapusKader" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Hapus Kader</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form method="post" id="form_hapus_kader">
				<div class="modal-body">
					<p>Apakah anda yakin akan menghapus data ini?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-danger btn-sm">Hapus</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	function editKader(id) {
		$.ajax({
			url: "<?php echo site_url('Kader/get_detail_kader/') ?>" + id,
			dataType: "JSON",
			success: function (data) {
				$("#editKader").modal("show");
				$("#form_edit_kader").attr("action", "<?php echo site_url('Kader/EditKader/') ?>" + id);
				$("#namakader").val(data.kader_nama);
				$("#userkader").val(data.username);
				$("#passkader").val(data.password);
				$("#tlpkader").val(data.kader_tlp);
			},

			error: function (jqXHR, textStatus, errorThrown) {
				alert('Gagal mengambil data');
			}
		});
	}

	function hapusKader(id) {
		$("#hapusKader").modal("show");
		$("#form_hapus_kader").attr("action", "<?php echo site_url('Kader/HapusKader/') ?>" + id);
	}

	function myFunction() {
		var x = document.getElementById("passkader");
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
	}
</script>

<?php echo $footer ?>

