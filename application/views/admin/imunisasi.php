<?php echo $header ?>
<div class="page has-sidebar-left">
	<?php if ($this->session->flashdata('message')){ ?>
		<div class="alert alert-dismissible <?= $this->session->flashdata('stts') == true ? 'alert-success bg-success' : 'alert-danger bg-danger' ?> text-white border-0 fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<?= $this->session->flashdata('message') ?>
		</div>
	<?php } ?>
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-eyedropper"></i> Data Imunisasi
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-lg-6">
				<button class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#exampleModal">Tambah</button>
				<div class="card">
					<div class="card-header white">
						<i class="icon-eyedropper blue-text"></i>
						<strong> Data Imunisasi </strong>
					</div>
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover data-tables" data-options='{ "paging": false; "searching":false}'>
							<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">Imunisasi</th>
								<th class="text-center">Jumlah</th>
								<!--								<th class="text-center">Aksi</th>-->
							</tr>
							</thead>
							<tbody>
							<?php $no=1; foreach($imunisasi->result() as $row) { ?>
								<tr>
									<td><?php echo $no ?></td>
									<td><?php echo $row->nama_imunisasi ?></td>
									<td><?php echo $row->jumlah ?></td>
									<!--									<td>-->
									<!--										<a href="#" class="btn btn-primary btn-xs">Detail</a>-->
									<!--									</td>-->
								</tr>
								<?php $no++; } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<form action="<?php echo site_url('imunisasi/TambahStok') ?>" method="post">
					<div class="card">
						<div class="card-header white">
							<i class="icon-plus blue-text"></i>
							<strong> Tambah Stok </strong>
						</div>
						<div class="card-body">
							<div class="form-body">
								<div class="form-group">
									<label for="" class="control-label">Jenis Imunisasi</label>
									<select name="jenis" class="form-control">
										<option value="">-- Pilih Jenis --</option>
										<?php foreach($imunisasi->result() as $row) { ?>
											<option value="<?php echo $row->id_imunisasi ?>">
												<?php echo $row->nama_imunisasi ?>
											</option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group">
									<label for="" class="control-label">Jumlah Stok</label>
									<input type="number" min="0" name="stok" class="form-control">
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button type="submit" class="btn btn-primary btn-sm">Tambah Stok</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?php echo base_url('Imunisasi/TambahImunisasi')?>" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Tambah Data Ibu</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Imunisasi</label>
							<input type="text" name="imunisasi" class="form-control" id="checknikibu" required>
							<span id="nik_result"></span>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Stok</label>
							<input type="number" name="stok" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Jenis</label>
							<select name="jenis" class="form-control" id="jenis">
								<option>--Pilih--</option>
								<option value="anak">Anak</option>
								<option value="ibu">Ibu</option>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm" id="simpan">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php echo $footer ?>
