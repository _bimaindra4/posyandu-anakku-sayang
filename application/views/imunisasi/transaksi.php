<?php echo $header ?>

<style>
	.menghilang {
		display: none;
	}
</style>

<div class="page has-sidebar-left">
	<?php if ($this->session->flashdata('message')) { ?>
		<div
			class="alert alert-dismissible <?= $this->session->flashdata('stts') == true ? 'alert-success bg-success' : 'alert-danger bg-danger' ?> text-white border-0 fade show"
			role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<?= $this->session->flashdata('message') ?>
		</div>
	<?php } ?>
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-eyedropper"></i> Transaksi Imunisasi
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-md-5">
				<div class="card">
					<div class="card-header white">
						<i class="icon-search blue-text"></i>
						<strong> Cari Data Anak </strong>
					</div>
					<div class="card-body">
						<form>
							<div class="form-body">
								<div class="form-group">
									<label for="" class="control-label">Pilih Pencarian</label>
									<select name="pilih" id="pilih" class="form-control" onchange="metode_pencarian(this.value)">
										<option value="">-- Pilih --</option>
										<option value="kms">Berdasarkan KMS Anak</option>
										<option value="nama">Berdasarkan Nama Anak</option>
									</select>
								</div>
								<div class="form-group menghilang" id="form_no_kms">
									<label for="" class="control-label">Masukkan No KMS</label>
									<input type="text" name="no_kms" id="no_kms" class="form-control">
								</div>
								<div class="form-group menghilang" id="form_nama">
									<label for="" class="control-label">Masukkan Nama Anak</label>
									<input type="text" name="nama_kms" id="nama_kms" class="form-control">
								</div>
							</div>
						</form>
					</div>
					<div class="card-footer">
						<div class="form-group">
							<button class="btn btn-primary" onclick="cari_anak()">Cari</button>
						</div>
					</div>
				</div>
				<div id="hasil-pencarian"></div>
			</div>
			<div class="col-md-7">
				<div id="result-anak"></div>
				<div class="card">
					<div class="card-header white menghilang">
						<i class="icon-search blue-text"></i>
						<strong> Historis Imunisasi </strong>
					</div>
					<div class="card-body">
						<p class="text-center" id="text-imunisasi-kosong">Data Belum Dipilih</p>
						<div class="menghilang" id="tabel-imunisasi">
							<button class="btn btn-sm btn-primary mb-3" onclick="tambah_imunisasi()" id="tambah-imunisasi"
											data-id="0">Tambah Imunisasi
							</button>
							<table class="table table-bordered table-hover">
								<thead>
								<tr>
									<th>No</th>
									<th>Jenis Imunisasi</th>
									<th>Tanggal</th>
								</tr>
								</thead>
								<tbody id="data-imun"></tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="tambahAnakImunisasi" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?php echo site_url('imunisasi/TambahAnakImunisasi') ?>" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Tambah Imunisasi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>KMS Anak</label>
							<input type="text" name="kms_anak" id="kms_anak" class="form-control" readonly>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Umur Anak (Bln)</label>
							<input type="number" step="1" min="0" name="umur_anak" class="form-control" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12 mb-3">
									<label>Berat Badan (Kg)</label>
									<input type="number" step="0.1" min="0" name="bb_anak" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12 mb-3">
									<label>Tinggi Badan (cm)</label>
									<input type="number" step="1" min="0" name="tb_anak" class="form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Pilih Imunisasi</label>
							<select name="imunisasi" class="form-control" id="imunisasi">
								<option value="">-- Pilih Imunisasi --</option>
								<?php foreach ($imunisasi->result() as $row) { ?>
									<option value="<?php echo $row->id_imunisasi ?>">
										<?php echo $row->nama_imunisasi . " (stok " . $row->jumlah . ")" ?>
									</option>
								<?php } ?>
								<option value="0">-Lainnya-</option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Imunisasi Lain</label>
							<input type="text" name="imunisasi_lain" class="form-control" id="imunisasilain" disabled="disabled">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php echo $footer ?>

<script>

  function tambah_imunisasi() {
    let btnTambah = $("#tambahAnakImunisasi");
    btnTambah.modal("show");
    $("#kms_anak").val($("#tambah-imunisasi").data("id"));
  }

  $(function () {
    $("#imunisasi").change(function () {
      if ($(this).val() == 0) {
        $("#imunisasilain").removeAttr("disabled");
        $("#imunisasilain").focus();
      } else {
        $("#imunisasilain").attr("disabled", "disabled");
      }
    });
  });

  function metode_pencarian(val) {
    if (val == "kms") {
      $("#form_no_kms").removeClass("menghilang");
      $("#form_nama").addClass("menghilang");
    } else if (val == "nama") {
      $("#form_no_kms").addClass("menghilang");
      $("#form_nama").removeClass("menghilang");
    } else {
      $("#form_no_kms").addClass("menghilang");
      $("#form_nama").addClass("menghilang");
    }
  }

  function cari_anak() {
    let pilih = $("#pilih").val();
    let val = "";

    if (pilih == "kms") {
      val = $("#no_kms").val();
    } else if (pilih == "nama") {
      val = $("#nama_kms").val();
    }

    $.ajax({
      url: "<?php echo site_url('imunisasi/find_anak_by_kms_nama/') ?>" + pilih + "/" + val,
      dataType: "JSON",
      success: function (data) {
        $("#hasil-pencarian").html(data.output);
      },
      error: function () {
        alert('Gagal mengambil data');
      }
    });
  }

  function get_data_anak(val) {
    $.ajax({
      url: "<?php echo site_url('imunisasi/get_data_anak/') ?>" + val,
      dataType: "JSON",
      success: function (data) {
        $("#result-anak").html(data.anak);
        $("#data-imun").html(data.imunisasi);
        $("#hasil-pencarian").html("");
        $("#text-imunisasi-kosong").addClass("menghilang");
        $("#tabel-imunisasi").removeClass("menghilang");
        $("#tambah-imunisasi").attr("data-id", data.kms_anak);
      },
      error: function () {
        alert('Gagal mengambil data');
      }
    });
  }

</script>
