<?php echo $header ?>
<div class="page has-sidebar-left">
	<?php if ($this->session->flashdata('message')){ ?>
		<div class="alert alert-dismissible <?= $this->session->flashdata('stts') == true ? 'alert-success bg-success' : 'alert-danger bg-danger' ?> text-white border-0 fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<?= $this->session->flashdata('message') ?>
		</div>
	<?php } ?>
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-eyedropper"></i> Data Imunisasi
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header white">
						<i class="icon-eyedropper blue-text"></i>
						<strong> Data Imunisasi </strong>
					</div>
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover data-tables" data-options='{ "paging": false; "searching":false}'>
							<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">Imunisasi</th>
								<th class="text-center">Jumlah</th>
<!--								<th class="text-center">Aksi</th>-->
							</tr>
							</thead>
							<tbody>
							<?php $no=1; foreach($imunisasi->result() as $row) { ?>
								<tr>
									<td><?php echo $no ?></td>
									<td><?php echo $row->nama_imunisasi ?></td>
									<td><?php echo $row->jumlah ?></td>
<!--									<td>-->
<!--										<a href="#" class="btn btn-primary btn-xs">Detail</a>-->
<!--									</td>-->
								</tr>
								<?php $no++; } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $footer ?>
