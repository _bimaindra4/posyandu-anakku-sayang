<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
				integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<style>
		@page {
			margin: 0px;
		}

		body {
			margin: 0px;
		}

		p {
			font-size: 12px
		}

		.table td, .table th {
			font-size: 10px;
			padding: .25rem;
			text-align: center;
			vertical-align: middle;
		}
	</style>
	<title></title>
</head>
<body>
<div id="header" class="mt-3">
	<p class="m-0 text-center">LAPORAN PENDATAAN OLEH KADER DI POSYANDU</p>
	<p class="m-0 text-center">KELURAHAN <b>KOTALAMA</b> RW: 7A</p>
</div>
<table class="table table-bordered mt-4 mx-3">
	<thead>
	<tr>
		<th rowspan=2>NO</th>
		<th rowspan=2>NIK</th>
		<th>NAMA WUS & PUS</th>
		<th rowspan=2>TGL LAHIR</th>
		<th rowspan=2>USIA</th>
		<th colspan=2>JML ANAK</th>
		<th rowspan=2 width=50>USIA ANAK TERAKHIR</th>
		<th colspan=2>KB</th>
		<th rowspan=2>KODE</th>
		<th rowspan=2>KETERANGAN</th>
	</tr>
	<tr>		
		<th>NAMA SUAMI</th>
		<th>L</th>
		<th>P</th>
		<th>JENIS</th>
		<th>LAMA</th>
	</tr>
	</thead>
	<tbody>
		<?php $no=1; foreach($laporan as $row) { ?>
			<tr>
				<td rowspan=2><?= $no ?></td>
				<td rowspan=2><?= $row->nik_ibu ?></td>
				<td><?= $row->nama_ibu ?></td>
				<td rowspan=2><?= $row->ibu_tgl_lahir ?></td>
				<td rowspan=2><?= $row->usia ?></td>
				<td rowspan=2><?= $row->anak_l ?></td>
				<td rowspan=2><?= $row->anak_p ?></td>
				<td rowspan=2><?= $row->usia_anak_terakhir ?></td>
				<td rowspan=2><?= $row->kb_jenis ?></td>
				<td rowspan=2><?= $row->kb_lama ?></td>
				<td rowspan=2><?= $row->kode ?></td>
				<td rowspan=2><?= $row->keterangan ?></td>
			</tr>
			<tr>
				<td><?= $row->suami_ibu ?></td>
			</tr>
		<?php $no++; } ?>
	</tbody>
</table>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
				integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
				crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
				integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
				crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
				integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
				crossorigin="anonymous"></script>
</body>
</html>
