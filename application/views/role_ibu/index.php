<?php echo $header ?>

<div class="page has-sidebar-left">
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-pages"></i>
						Data Diri Ibu
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="card">
			<div class="card-body">
				<div class="tab-content">
					<table class="table table-bordered table-hover">
						<tr>
							<td class="heading" width="200">Nama Ibu</td>
							<td><?php echo $ibu->nama_ibu ?></td>
						</tr>
						<tr>
							<td class="heading">Nama Suami</td>
							<td><?php echo $ibu->suami_ibu ?></td>
						</tr>
						<tr>
							<td class="heading">Tanggal Lahir</td>
							<td><?php echo date("d/m/Y", strtotime($ibu->ibu_tgl_lahir)) ?></td>
						</tr>
						<tr>
							<td class="heading">Umur</td>
							<td><?php echo $ibu->ibu_umur ?></td>
						</tr>
						<tr>
							<td class="heading">No Telepon</td>
							<td><?php echo $ibu->ibu_telpon ?></td>
						</tr>
						<tr>
							<td class="heading">Status</td>
							<td>
								<?php
								if ($ibu->status_ibu == 1) {
									echo "Hamil";
								} else if ($ibu->status_ibu == 2) {
									echo "PUS / WUS";
								} else if ($ibu->status_ibu == 3) {
									echo "Melahirkan";
								}
								?>
							</td>
						</tr>
						<tr>
							<td class="heading">Umur Kehamilan (Minggu)</td>
							<td><?php if ($ibu->status_ibu == 1) {
									echo calWeek($ibu->tanggal_kehamilan);
								} else if ($ibu->status_ibu == 3) {
									echo "-";
								}
								?></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $footer ?>
