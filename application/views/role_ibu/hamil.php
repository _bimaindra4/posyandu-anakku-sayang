<?php echo $header ?>

<style>
	td.heading {
		font-weight: 400;
	}
</style>

<div class="page has-sidebar-left">
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-child_friendly "></i> Data Ibu Hamil
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-lg-3">
				<div class="card">
					<div class="card-body text-center">
						<div class="image m-3">
							<img class="user_avatar no-b no-p r-5" src="<?php echo base_url() ?>assets/img/ibu.png">
						</div>
						<div>
							<h6 class="p-t-10"><?php echo $ibu->nama_ibu ?></h6>
							<?php echo $ibu->nik_ibu ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="align-self-center">
					<ul class="nav nav-pills mb-3" role="tablist">
						<li class="nav-item">
							<a class="nav-link active show" data-toggle="tab" href="#tabInformasi" role="tab" aria-controls="tab1" aria-expanded="true" aria-selected="true">Informasi</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#tabImunisasi" role="tab" aria-controls="tab2" aria-selected="false">Histori Konsultasi</a>
						</li>	
					</ul>
				</div>
				<div class="card">
					<div class="card-body">
						<div class="tab-content">
							<div class="tab-pane fade show active" id="tabInformasi" role="tabpanel" aria-labelledby="tabInformasi">
								<table class="table table-bordered table-hover">
									<tr>
										<td class="heading" width="200">Alamat Ibu</td>
										<td><?php echo $ibu->alamat_ibu ?></td>
									</tr>
                                    <tr>
                                        <td class="heading">Nama Suami</td>
                                        <td><?php echo $ibu->suami_ibu ?></td>
                                    </tr>
                                    <tr>
                                        <td class="heading">Tanggal Lahir</td>
                                        <td><?php echo date("d/m/Y", strtotime($ibu->ibu_tgl_lahir)) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="heading">Umur</td>
                                        <td><?php echo $ibu->ibu_umur ?></td>
                                    </tr>
                                    <tr>
                                        <td class="heading">No Telepon</td>
                                        <td><?php echo $ibu->ibu_telpon ?></td>
                                    </tr>
                                    <tr>
                                        <td class="heading">Status</td>
                                        <td>
                                            <?php
                                                if($ibu->status_ibu == 1) {
                                                    echo "Hamil";
                                                } else if($ibu->status_ibu == 2) {
                                                    echo "PUS / WUS";
                                                }
                                            ?>
                                        </td>
                                    </tr>
									<tr>
										<td class="heading">Umur Kehamilan (Minggu)</td>
										<td><?php echo calWeek($ibu->tanggal_kehamilan) ?></td>
									</tr>
								</table>
								<table class="table table-bordered table-hover">
<!--									<tr>-->
<!--										<td width="200" class="heading">Hari Pertama Haid Terakhir</td>-->
<!--										<td colspan="3">--><?php //echo date("d/m/Y", strtotime($ibu->tanggal_hpht)) ?><!--</td>-->
<!--									</tr>-->
<!--									<tr>-->
<!--										<td class="heading">Hari Taksiran Persalinan</td>-->
<!--										<td colspan="3">--><?php //echo date("d/m/Y", strtotime($ibu->tanggal_htp)) ?><!--</td>-->
<!--									</tr>-->
<!--									<tr>-->
<!--										<td class="heading">Lingkar Lengan Atas</td>-->
<!--										<td colspan="3">--><?php //echo $ibu->lingkar_lengan." cm" ?><!--</td>-->
<!--									</tr>-->
									<tr>
										<td class="heading">Berat Badan</td>
										<td colspan="3"><?php echo $ibu->berat_badan." kg" ?></td>
									</tr>
									<tr>
										<td class="heading">Penggunaan Kontrasepsi Sebelum Kehamilan Ini</td>
										<td colspan="3"><?php echo $ibu->penggunaan_kontrasepsi ?></td>
									</tr>
<!--									<tr>-->
<!--										<td class="heading">Riwayat Penyakit</td>-->
<!--										<td colspan="3">--><?php //echo $ibu->riwayat_penyakit ?><!--</td>-->
<!--									</tr>-->
<!--									<tr>-->
<!--										<td class="heading">Riwayat Alergi</td>-->
<!--										<td colspan="3">--><?php //echo $ibu->riwayat_alergi ?><!--</td>-->
<!--									</tr>-->
<!--									<tr>-->
<!--										<td class="heading">Hamil ke</td>-->
<!--										<td colspan="3">--><?php //echo $ibu->hamil_ke ?><!--</td>-->
<!--									</tr>-->
									<tr>
										<td class="heading">Jumlah Persalinan</td>
										<td><?php echo $ibu->jml_persalinan ?></td>
										<td width="200" class="heading">Jumlah Keguguran</td>
										<td><?php echo $ibu->jml_keguguran ?></td>
									</tr>
									<tr>
										<td class="heading">Jumlah Anak Hidup</td>
										<td><?php echo $ibu->jml_anak_hidup ?></td>
										<td class="heading">Jumlah Lahir Mati</td>
										<td><?php echo $ibu->jml_lahir_mati ?></td>
									</tr>
<!--									<tr>-->
<!--										<td class="heading">Jumlah Anak Lahir Kurang Bulan</td>-->
<!--										<td colspan="3">--><?php //echo $ibu->jml_anak_lahir_kurang_bulan ?><!--</td>-->
<!--									</tr>-->
<!--									<tr>-->
<!--										<td class="heading">Jarak Kehamilan ini dengan Persalinan Terakhir</td>-->
<!--										<td colspan="3">--><?php //echo $ibu->jarak_kehamilan ?><!--</td>-->
<!--									</tr>-->
									<tr>
										<td class="heading">Status Imunisasi TT</td>
										<td><?php echo $ibu->status_imun_tt ?></td>
										<td class="heading">Imunisasi TT Terakhir</td>
										<td><?php echo date("d/m/Y", strtotime($ibu->imun_tt_terakhir)) ?></td>
									</tr>
									<tr>
										<td class="heading">Penolong Persalinan Terakhir</td>
										<td colspan="3"><?php echo $ibu->penolong_persalinan ?></td>
									</tr>
									<tr>
										<td class="heading">Cara Persalinan Terakhir</td>
										<td colspan="3"><?php echo $ibu->cara_persalinan ?></td>
									</tr>
								</table>
							</div>
							<div class="tab-pane fade" id="tabImunisasi" role="tabpanel" aria-labelledby="tabImunisasi">
								<table id="example2" class="table table-bordered table-hover data-tables" data-options='{ "paging": false; "searching":false}'>
									<thead>
									<tr>
										<th>No</th>
										<th>Tanggal Pelayanan</th>
										<th>Keluhan</th>
										<th>Umur Kehamilan</th>
									</tr>
									</thead>
									<tbody>
										<?php $no=1; foreach($konsultasi->result() as $row) { ?>
											<tr>
												<td><?php echo $no ?></td>
												<td><?php echo date("d/m/Y", strtotime($row->tgl_pelayanan)) ?></td>
												<td><?php echo $row->keluhan ?></td>
												<td><?php echo calWeek($row->tanggal_kehamilan)." minggu" ?></td>
											</tr>
										<?php $no++; } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $footer ?>
