<?php echo $header ?>
<div class="page has-sidebar-left">
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-child_friendly"></i> Data Ibu PUS/WUS
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header white">
						<i class="icon-child_friendly blue-text"></i>
						<strong> Data PUS / WUS </strong>
					</div>
					<div class="card-body">
                        <table id="example2" class="table table-bordered table-hover data-tables" data-options='{ "paging": false; "searching":false}'>
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Pelayanan</th>
                                    <th>Imunisasi</th>
                                    <th>Usia Anak Terakhir</th>
                                    <th>KB Jenis</th>
                                    <th>KB Lama</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($konsultasi->result() as $row) { ?> 
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $row->tgl_pelayanan ?></td>
                                        <td><?= $row->imunisasi ?></td>
                                        <td><?= $row->usia_anak_terakhir." th" ?></td>
                                        <td><?= $row->kb_jenis ?></td>
                                        <td><?= $row->kb_lama ?></td>
                                    </tr>    
                                <?php } ?>
                            </tbody>
                        </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $footer ?>