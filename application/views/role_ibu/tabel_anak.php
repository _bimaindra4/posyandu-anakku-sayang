<?php echo $header ?>

<div class="page has-sidebar-left">
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-child_care"></i> Data Anak
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-lg-12">  
				<div class="card ">
					<div class="card-header white">
						<i class="icon-clipboard-edit blue-text"></i>
						<strong> Data Anak </strong>
					</div>
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover data-tables" data-options='{ "paging": false; "searching":false}'>
							<thead>
								<tr>
									<th class="text-center">No</th>
									<th class="text-center">KMS Anak</th>
									<th class="text-center">Nama Anak</th>
									<th class="text-center">Tempat, Tanggal Lahir</th>
									<th class="text-center">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach($anak->result() as $row) { ?>
								<tr>
									<td class="text-center"><?php echo $no ?></td>
									<td class="text-center"><?php echo $row->kms_anak ?></td>
									<td class="text-center"><?php echo $row->nama_anak ?></td>
									<td class="text-center">
										<?php
											$tempat = $row->anak_tmpt_lahir;
											$tgl = $row->anak_tgl_lahir;
											if($tempat == NULL && $tgl != NULL) {
												echo $tgl;
											} else if($tempat != NULL && $tgl == NULL) {
												echo $tempat;
											} else if($tempat != NULL && $tgl != NULL) {
												echo $tempat.", ".$tgl;
											} else {
												echo '<span class="badge r-3 badge-danger">Tidak ada data</span>';
											}
										?>
									</td>
									<td class="text-center">
										<a href="<?php echo base_url()."anak/detail/".$row->id_anak ?>" class="btn btn-primary btn-xs">Detail</a>
									</td>
								</tr>
								<?php $no++; } ?>
							</tbody>
						</table>
					</div>
				</div>   
			</div>
		</div>
	</div>
</div>

<?php echo $footer ?>
