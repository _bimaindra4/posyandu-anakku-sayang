<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
				integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<style>
		@page {
			margin: 0px;
		}

		body {
			margin: 0px;
		}

		p {
			font-size: 12px
		}

		.table td, .table th {
			font-size: 10px;
			padding: .25rem;
			text-align: center;
			vertical-align: middle;
		}
	</style>
	<title><?= $title ?></title>
</head>
<body>
<div id="header" class="mt-3">
	<p class="m-0 text-center">LAPORAN PENDATAAN OLEH KADER DI POSYANDU</p>
	<p class="m-0 text-center">KELURAHAN <b>KOTALAMA</b> RW: 7A</p>
	<p class="m-0 text-center">BULAN <?= strtoupper($month_text) ?> TAHUN <?= $year ?></p>
</div>
<table class="table table-bordered mt-4 mx-3">
	<thead>
	<tr>
		<th colspan=2 width="200px">NAMA</th>
		<th rowspan=2>UMUR IBU</th>
		<th rowspan=2>ALAMAT</th>
		<th rowspan=2>RT</th>
		<th rowspan=2>NIK</th>
		<th rowspan=2>HAMIL KE</th>
		<th rowspan=2>USIA KEHAMILAN</th>
		<th rowspan=2>HAID TERAKHIR</th>
		<th rowspan=2>LILA</th>
		<th rowspan=2>TEMPAT PERIKSA HAMIL</th>
		<th rowspan=2>JENIS RESIKO TINGGI BILA ADA</th>
		<th rowspan=2>NOMOR HP</th>
		<th colspan=2>PUNYA BPJS/KIS</th>
	</tr>
	<tr>
		<th>IBU</th>
		<th>SUAMI</th>
		<th>Y</th>
		<th>T</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($laporan as $row) { ?>
		<tr>
			<td><?= $row->nama_ibu ?></td>
			<td><?= $row->suami_ibu ?></td>
			<td><?= $row->ibu_umur . " Th" ?></td>
			<td><?= $row->alamat_ibu ?></td>
			<td><?= $row->rt ?></td>
			<td><?= $row->nik_ibu ?></td>
			<td><?= $row->hamil_ke ?></td>
			<td><?= $row->umur_kehamilan ?></td>
			<td><?= $row->hpht ?></td>
			<td><?= $row->lila ?></td>
			<td><?= $row->tempat_periksa_hamil ?></td>
			<td><?= $row->jenis_resiko_tinggi ?></td>
			<td><?= $row->ibu_telpon ?></td>
			<!--                        <td>--><? //= $row->bpjs_kis ?><!--</td>-->
			<td><?= ($row->bpjs_kis == "ya" ? "v" : "") ?></td>
			<td><?= ($row->bpjs_kis == "tidak" ? "v" : "") ?></td>
		</tr>
	<?php } ?>
	</tbody>
</table>

<!--<table class="table table-bordered mt-4 mx-3">
	<thead>
	<tr>
		<th colspan=2 width="200px">NAMA</th>
		<th rowspan=2>UMUR IBU</th>
		<th rowspan=2>ALAMAT</th>
		<th rowspan=2>RT</th>
		<th rowspan=2>NIK</th>
		<th rowspan=2>ANAK KE</th>
		<th rowspan=2>NAMA BAYI</th>
		<th rowspan=2>L/P</th>
		<th rowspan=2>TANGGAL MELAHIRKAN</th>
		<th rowspan=2>TEMPAT BERSALIN</th>
		<th rowspan=2>PERSALINAN (NORMAL/OPERASI)</th>
		<th colspan=2>BERAT LAHIR</th>
		<th rowspan=2>KEADAAN IBU</th>
		<th rowspan=2>KEADAAN BAYI (NORMAL/BERMASALAH)</th>
		<th colspan=2>PUNYA BPJS/KIS</th>
	</tr>
	<tr>
		<th>IBU</th>
		<th>SUAMI</th>
		<th colspan="2">PANJANG</th>
		<th>Y</th>
		<th>T</th>
	</tr>
	</thead>
	<tbody>
	<?php /*foreach ($laporan_anak as $row) { */?>
		<tr>
			<td><?/*= $row->nama_ibu */?></td>
			<td><?/*= $row->suami_ibu */?></td>
			<td><?/*= $row->ibu_umur . " Th" */?></td>
			<td><?/*= $row->alamat_ibu */?></td>
			<td><?/*= $row->rt */?></td>
			<td><?/*= $row->nik_ibu */?></td>
			<td>-</td>
			<td><?/*= $row->nama_anak */?></td>
			<td><?/*= $row->kelamin */?></td>
			<td><?/*= $row->anak_tgl_lahir */?></td>
			<td>-</td>
			<td colspan="2"><?/*= $row->anak_bb_lahir */?></td>
			<td>-</td>
			<td>-</td>
			<td><?/*= ($row->bpjs_kis == "ya" ? "v" : "") */?></td>
			<td><?/*= ($row->bpjs_kis == "tidak" ? "v" : "") */?></td>
		</tr>
	<?php /*} */?>
	</tbody>
</table>-->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
				integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
				crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
				integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
				crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
				integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
				crossorigin="anonymous"></script>
</body>
</html>
