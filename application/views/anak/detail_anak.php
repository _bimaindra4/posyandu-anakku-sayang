<?php echo $header ?>

<div class="page has-sidebar-left">
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-child_care"></i> Detail Anak
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-md-12">
				<a href="<?php echo site_url('anak') ?>" class="btn btn-primary btn-sm mb-3">Kembali</a>
			</div>
			<div class="col-lg-3">
				<div class="card">
					<div class="card-body text-center">
						<div class="image m-3">
							<img class="user_avatar no-b no-p r-5" src="<?php echo base_url() ?>assets/img/baby.png">
						</div>
						<div>
							<h6 class="p-t-10"><?php echo $anak->nama_anak ?></h6>
							<?php echo $anak->kms_anak ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="align-self-center">
					<ul class="nav nav-pills mb-3" role="tablist">
						<li class="nav-item">
							<a class="nav-link active show" data-toggle="tab" href="#tabInformasi" role="tab" aria-controls="tab1"
								 aria-expanded="true" aria-selected="true">Informasi</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#tabImunisasi" role="tab" aria-controls="tab2"
								 aria-selected="false">Histori Imunisasi</a>
						</li>
					</ul>
				</div>
				<div class="tab-content">
					<div class="tab-pane fade show active" id="tabInformasi" role="tabpanel" aria-labelledby="tabInformasi">
						<div class="card">
							<div class="card-body">
								<table class="table table-bordered table-hover">
									<tr>
										<td width="200">Tempat, Tanggal Lahir</td>
										<td>
											<?php
											$tempat = $anak->anak_tmpt_lahir;
											$tgl = $anak->anak_tgl_lahir;
											if ($tempat == NULL && $tgl != NULL) {
												echo date("d/m/Y", strtotime($tgl));
											} else if ($tempat != NULL && $tgl == NULL) {
												echo $tempat;
											} else if ($tempat != NULL && $tgl != NULL) {
												echo $tempat . ", " . date("d/m/Y", strtotime($tgl));
											} else {
												echo '<span class="badge r-3 badge-danger">Tidak ada data</span>';
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Berat Badan Lahir</td>
										<td>
											<?php
											$bb = $anak->anak_bb_lahir;
											if ($bb == NULL) {
												echo '<span class="badge r-3 badge-danger">Tidak ada data</span>';
											} else {
												echo $bb . " Kg";
											}
											?>
										</td>
									</tr>
									<tr>
										<td>Jenis Kelamin</td>
										<td>
											<?php
											$jkel = $anak->anak_kelamin;
											if ($jkel == NULL) {
												echo '<span class="badge r-3 badge-danger">Tidak ada data</span>';
											} else {
												echo $jkel;
											}
											?>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="tabImunisasi" role="tabpanel" aria-labelledby="tabImunisasi">
						<div style="height: 350px">
							<?php
							$arrBulan = "[";
							$arrBB = "[";
							foreach ($bb_anak as $row) {
								$arrBulan .= "'" . $row->umur_anak . "', ";
								$arrBB .= "'" . $row->bb_anak . "', ";
							}
							$arrBulan = rtrim($arrBulan, ", ");
							$arrBulan .= "]";
							$arrBB = rtrim($arrBB, ", ");
							$arrBB .= "]";
							?>
							<canvas
								data-chart="line"
								data-dataset="[<?= $arrBB ?>]"
								data-labels="<?= $arrBulan ?>"
								data-dataset-options="datasets: [{
										label: 'BB Anak (Kg)',
										fill: !0,
										lineTension: 0,
										backgroundColor: 'rgba(0,172,255, 0.1)',
										borderWidth: 2,
										borderColor: '#00AAFF',
										borderCapStyle: 'butt',
										borderDash: [],
										borderDashOffset: 0,
										borderJoinStyle: 'miter',
										pointRadius: 2,
										pointBorderColor: '#00AAFF',
										pointBackgroundColor: '#fff',
										pointBorderWidth: 2,
										pointHoverRadius: 6,
										pointHoverBackgroundColor: '#fff',
										pointHoverBorderColor: '#00AAFF',
										pointHoverBorderWidth: 2,
										data: <?= $arrBB ?>,
										spanGaps: !1
									}]"
								data-options="{
										legend: {
											display: !0,
											labels: {
												fontColor: '#7F8FA4',
												fontFamily: 'Source Sans Pro, sans-serif',
												boxRadius: 4,
												usePointStyle: !0
											}
										},
										layout: {
											padding: {
												left: 0,
												right: 0,
												top: 0,
												bottom: 0
											}
										},
										scales: {
											xAxes: [{
												display: !0,
												ticks: {
													fontSize: '11',
													fontColor: '#969da5'
												},
												gridLines: {
													color: 'rgba(0,0,0,0.0)',
													zeroLineColor: 'rgba(0,0,0,0.0)'
												}
											}],
											yAxes: [{
												display: !0,
												gridLines: {
													color: 'rgba(223,226,229,0.45)',
													zeroLineColor: 'rgba(0,0,0,0.0)'
												},
												ticks: {
													beginAtZero: !0,
													max: 20,
													stepSize: 1,
													fontSize: '11',
													fontColor: '#969da5'
												}
											}]
										}
									}">
							</canvas>
						</div>
						<div class="card mt-3">
							<div class="card-body">
								<table id="example2" class="table table-bordered table-hover data-tables"
											 data-options='{ "paging": false; "searching":false}'>
									<thead>
									<tr>
										<th>No</th>
										<th>Tanggal Pelayanan</th>
										<th>Umur Anak (Minggu)</th>
										<th>Imunisasi</th>
										<th>Berat Badan</th>
										<th>Tinggi Badan</th>
										<th>Aksi</th>
									</tr>
									</thead>
									<tbody>
									<?php $no = 1;
									foreach ($histori as $row) { ?>
										<tr>
											<td><?php echo $no ?></td>
											<td><?php echo date("d/m/Y", strtotime($row['tgl_pelayanan'])) ?></td>
											<td><?php echo $row['umur_anak'] ?></td>
											<td><?php echo $row['nama_imunisasi'] ?></td>
											<td><?php echo $row['bb_anak'] . " Kg" ?></td>
											<td><?php echo $row['tb_anak'] . " cm" ?></td>
											<td>
												<button onclick="editTransaksi('<?php echo $row['id_lay_anak'] ?>')"
																class="btn btn-warning btn-xs">Edit
												</button>
											</td>
										</tr>
										<?php $no++;
									} ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $footer ?>

<script>
	function editTransaksi(val) {
		$.ajax({
			url: "<?php echo site_url('Anak/get_layanan_anak/'.$this->uri->segment(3).'/') ?>" + val,
			dataType: "JSON",
			success: function (data) {
				$("#editTransaksi").modal("show");
				//$("#formEditKonsultasi").attr("action", "<?php //echo base_url('Ibu/EditLayananIbu/') ?>//"+data.id_lay_bumil);
				//$("#imunisasi_ed").val(data.id_imunisasi);
				//$("#keluhan_ed").val(data.keluhan);
				//$("#tek_darah_ed").val(data.tekanan_darah);
				//$("#bb_ed").val(data.berat_badan);
				//$("#bb_lahir_ed").val(data.anak_bb_lahir);
				//$("#umur_ed").val(data.umur_kehamilan);
				//$("#tinggi_fundus_ed").val(data.tinggi_fundus);
				//$("#letak_janin_ed").val(data.letak_janin);
				//$("#detak_jantung_ed").val(data.denyut_jantung_janin);
				//$("#kaki_bengkak_ed").val(data.kaki_bengkak);
				//$("#hasil_lab_ed").text(data.hasil_lab);
				//$("#tindakan_ed").val(data.tindakan);
				//$("#nasihat_ed").text(data.nasihat);
				//$("#keterangan_ed").text(data.keterangan);
			},

			error: function (jqXHR, textStatus, errorThrown) {
				alert('Gagal mengambil data');
			}
		});
	}
</script>
