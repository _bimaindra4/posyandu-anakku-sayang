<?php echo $header ?>

<div class="page has-sidebar-left">
    <header class="my-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <h1 class="s-24">
                        <i class="icon-pages"></i>
                       Dashboard
                    </h1>
                </div>
            </div>
        </div>
    </header>
    <div class="container-fluid my-3">
        <div class="row">
            <div class="col-lg-3">
                <div class="counter-box p-40 gradient  text-white shadow2 r-5">
                    <div class="float-right">
                        <span class="icon icon-child_care s-48"></span>
                    </div>
                    <div class="s-36"><?php echo $anak ?></div>
                    <h6 class="counter-title">Anak</h6>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="counter-box p-40 gradient  text-white shadow2 r-5">
                    <div class="float-right">
                        <span class="icon icon-child_friendly s-48"></span>
                    </div>
                    <div class="s-36"><?php echo $ibu ?></div>
                    <h6 class="counter-title">Ibu</h6>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="counter-box p-40 gradient  text-white shadow2 r-5">
                    <div class="float-right">
                        <span class="icon icon-eyedropper s-48"></span>
                    </div>
                    <div class="s-36"><?php echo $imunisasi ?></div>
                    <h6 class="counter-title">Imunisasi</h6>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $footer ?>