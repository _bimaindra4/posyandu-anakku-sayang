<?php echo $header ?>
<div class="page has-sidebar-left">
	<?php if ($this->session->flashdata('message')){ ?>
		<div class="alert alert-dismissible <?= $this->session->flashdata('stts') == true ? 'alert-success bg-success' : 'alert-danger bg-danger' ?> text-white border-0 fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<?= $this->session->flashdata('message') ?>
		</div>
	<?php } ?>
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-child_friendly"></i> Data Ibu
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-lg-12">
				<button class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#exampleModal">Tambah</button>
				<div class="card ">
					<div class="card-header white">
						<i class="icon-child_friendly blue-text"></i>
						<strong> Data Ibu </strong>
					</div>
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover data-tables" data-options='{ "paging": false; "searching":false}'>
							<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">NIK</th>
								<th class="text-center">Nama Ibu</th>
								<th class="text-center">Nama Suami</th>
								<th class="text-center">Tempat, Tanggal Lahir</th>
								<th class="text-center">Status</th>
								<th class="text-center">Aksi</th>
							</tr>
							</thead>
							<tbody>
							<?php $no=1; foreach($ibu->result() as $row) { ?>
								<tr>
									<td class="text-center"><?php echo $no ?></td>
									<td><?php echo $row->nik_ibu ?></td>
									<td><?php echo $row->nama_ibu ?></td>
									<td><?php echo $row->suami_ibu ?></td>
									<td><?php echo $row->ibu_tempat_lahir.", ".date("d/m/Y", strtotime($row->ibu_tgl_lahir)) ?></td>
									<td><?php if ($row->status_ibu == 1) {
											echo "Hamil";
										} else if ($row->status_ibu == 2){
											echo "PUS/WUS";
										} else if ($row->status_ibu == 3){
											echo "Melahirkan";
										}	?>
									</td>
									<td class="text-center">
										<button onclick="editnikIbu('<?php echo $row->id_ibu ?>')" class="btn btn-primary btn-xs">Edit NIK</button>
										<button onclick="editIbu('<?php echo $row->id_ibu ?>')" class="btn btn-warning btn-xs">Edit Data</button>
									</td>
								</tr>
								<?php $no++; } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?php echo base_url('Ibu/TambahIbu')?>" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Tambah Data Ibu</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>NIK</label>
							<input type="number" name="nik_ibu" class="form-control" id="checknikibu" required>
							<span id="nik_result"></span>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Nama Ibu</label>
							<input type="text" name="nama_ibu" class="form-control" required>
						</div>
<!--					</div>-->
<!--					<div class="form-row">-->
						<div class="col-md-6 mb-3">
							<label>Nama Suami</label>
							<input type="text" name="nama_suami" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Tanggal Lahir Ibu</label>
							<input type="date" name="tgl_lahir" class="form-control" required>
						</div>
						<!--					</div>-->
						<!--					<div class="form-row">-->
						<div class="col-md-6 mb-3">
							<label>Tempat Lahir</label>
							<input type="text" name="tempat_lahir" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Nomor Telepon</label>
							<input type="text" name="nomor_tlp" class="form-control" required>
						</div>
						<div class="col-md-6 mb-3">
							<label>RT</label>
							<input type="text" name="rt" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Alamat Ibu</label>
							<input type="text" name="alamat" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Status</label>
							<select name="status" class="form-control" id="status">
								<option>--Pilih--</option>
								<option value="1">Hamil</option>
								<option value="2">WUS/PUS</option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Tanggal Kehamilan</label>
							<input type="date" name="tanggal_kehamilan" class="form-control" id="tanggalkehamilan" disabled="disabled">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm" id="simpan">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="EditIbu" action="" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Edit Data Ibu</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nama Ibu</label>
							<input type="text" name="nama_ibu" id="namaibu" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nama Suami</label>
							<input type="text" name="nama_suami" id="suami" class="form-control" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12 mb-3">
									<label>Tanggal Lahir Ibu</label>
									<input type="date" id="lahir" name="tgl_lahir" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12 mb-3">
									<label>Tempat Lahir</label>
									<input type="text" id="tempatlahir" name="tempat_lahir" class="form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Nomor Telepon</label>
							<input type="text" name="nomor_tlp" id="telpon" class="form-control" required>
						</div>
						<div class="col-md-6 mb-3">
							<label>RT</label>
							<input type="text" id="rt" name="rt" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Alamat Ibu</label>
							<input type="text" name="alamat" id="alamat" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Status</label>
							<select name="status" class="form-control" id="statusedit">
								<option>--Pilih--</option>
								<option value="1">Hamil</option>
								<option value="2">WUS/PUS</option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Tanggal Kehamilan</label>
							<input type="date" name="tanggal_kehamilan" class="form-control" id="tanggalkehamilanedit" disabled="disabled">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="editNikModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="EditNikIbu" action="" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Edit NIK Ibu</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>NIK</label>
							<input type="text" name="nik_ibu" id="nikibu" class="form-control" required>
							<span id="nikresult"></span>
						</div>
					</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
  $(function () {
    $("#status").change(function () {
      if ($(this).val() == 1) {
        $("#umurkehamilan").removeAttr("disabled");
		$("#tanggalkehamilan").removeAttr("disabled");
        $("#umurkehamilan").focus();
      } else {
        $("#umurkehamilan").attr("disabled", "disabled");
		$("#tanggalkehamilan").attr("disabled", "disabled");
      }
    });
  });

  $(function () {
    $("#statusedit").change(function () {
      if ($(this).val() == 1) {
        $("#umurkehamilanedit").removeAttr("disabled");
        $("#tanggalkehamilanedit").removeAttr("disabled");
        $("#umurkehamilanedit").focus();
      } else {
        $("#umurkehamilanedit").attr("disabled", "disabled");
        $("#tanggalkehamilanedit").attr("disabled", "disabled");
      }
    });
  });

	function editIbu(id){
		$('#EditIbu').attr("action","<?= base_url('ibu/EditIbu/') ?>" + id);
		$.ajax({
			url: "<?= base_url('ibu/detail_ibu/') ?>" + id,
			method: "GET",
			dataType: "JSON",
			success: function(data){
				$("#namaibu").val(data.nama_ibu);
				$("#suami").val(data.suami_ibu);
				$("#lahir").val(data.ibu_tgl_lahir);
				$("#tempatlahir").val(data.ibu_tempat_lahir);
				$("#rt").val(data.rt);
				$("#telpon").val(data.ibu_telpon);
				$("#alamat").val(data.alamat_ibu);
			}
		});
		$('#editModal').modal('show');
	}

  function editnikIbu(id){
    $('#EditNikIbu').attr("action","<?= base_url('ibu/EditNikIbu/') ?>" + id);
    $.ajax({
      url: "<?= base_url('ibu/detail_ibu/') ?>" + id,
      method: "GET",
      dataType: "JSON",
      success: function(data){
        $("#nikibu").val(data.nik_ibu);
      }
    });
    $('#editNikModal').modal('show');
  }

  $(document).ready(function(){
    $('#checknikibu').change(function(){
      var nik_ibu = $('#checknikibu').val();
      if(nik_ibu != '')
      {
        $.ajax({
          url:"<?php echo site_url('Ibu/check_nik_avalibility'); ?>",
          method:"POST",
          data:{nik_ibu:nik_ibu},
          success:function(data){
            $('#nik_result').html(data);
          }
        });
      }
    });
  });

	$(document).ready(function(){
		$('#nikibu').change(function(){
			var nik_ibu = $('#nikibu').val();
			if(nik_ibu != '')
			{
				$.ajax({
					url:"<?php echo site_url('Ibu/check_nik_avalibility'); ?>",
					method:"POST",
					data:{nik_ibu:nik_ibu},
					success:function(data){
						$('#nikresult').html(data);
					}
				});
			}
		});
	});
</script>

<?php echo $footer ?>
