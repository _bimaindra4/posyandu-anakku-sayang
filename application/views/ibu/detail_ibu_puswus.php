<?php echo $header ?>

<style>
	td.heading {
		font-weight: 400;
	}
</style>

<div class="page has-sidebar-left">
	<?php if ($this->session->flashdata('message')){ ?>
		<div class="alert alert-dismissible <?= $this->session->flashdata('stts') == true ? 'alert-success bg-success' : 'alert-danger bg-danger' ?> text-white border-0 fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<?= $this->session->flashdata('message') ?>
		</div>
	<?php } ?>
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-child_friendly "></i> Detail Ibu
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-md-12">
				<a href="<?php echo site_url('ibu/pus_wus') ?>" class="btn btn-primary btn-sm mb-3">Kembali</a>
			</div> 
			<div class="col-lg-3">
				<div class="card">
					<div class="card-body text-center">
						<div class="image m-3">
							<img class="user_avatar no-b no-p r-5" src="<?php echo base_url() ?>assets/img/ibu.png">
						</div>
						<div>
							<h6 class="p-t-10"><?php echo $ibu->nama_ibu ?></h6>
							<?php echo $ibu->nik_ibu ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="align-self-center">
					<ul class="nav nav-pills mb-3" role="tablist">
						<li class="nav-item">
							<a class="nav-link active show" data-toggle="tab" href="#tabInformasi" role="tab" aria-controls="tab1" aria-expanded="true" aria-selected="true">Informasi</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#tabImunisasi" role="tab" aria-controls="tab2" aria-selected="false">Histori Konsultasi</a>
						</li>	
					</ul>
				</div>
				<div class="card">
					<div class="card-body">
						<div class="tab-content">
							<div class="tab-pane fade show active" id="tabInformasi" role="tabpanel" aria-labelledby="tabInformasi">
								<table class="table table-bordered table-hover">
									<tr>
										<td class="heading" width="200">Alamat Ibu</td>
										<td><?php echo $ibu->alamat_ibu ?></td>
									</tr>
                                    <tr>
                                        <td class="heading">Nama Suami</td>
                                        <td><?php echo $ibu->suami_ibu ?></td>
                                    </tr>
                                    <tr>
                                        <td class="heading">Tanggal Lahir</td>
                                        <td><?php echo date("d/m/Y", strtotime($ibu->ibu_tgl_lahir)) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="heading">Umur</td>
                                        <td><?php echo $ibu->ibu_umur ?></td>
                                    </tr>
                                    <tr>
                                        <td class="heading">No Telepon</td>
                                        <td><?php echo $ibu->ibu_telpon ?></td>
                                    </tr>
                                    <tr>
                                        <td class="heading">Status</td>
                                        <td>
                                            <?php
                                                if($ibu->status_ibu == 1) {
                                                    echo "Hamil";
                                                } else if($ibu->status_ibu == 2) {
                                                    echo "PUS / WUS";
                                                }
                                            ?>
                                        </td>
                                    </tr>
									<tr>
<!--										<td class="heading">Umur Kehamilan (Minggu)</td>-->
<!--										<td>--><?php //echo $ibu->umur_kehamilan ?><!--</td>-->
									</tr>
								</table>
							</div>
							<div class="tab-pane fade" id="tabImunisasi" role="tabpanel" aria-labelledby="tabImunisasi">
								<button class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#exampleModal">Tambah Data</button>
								<table id="example2" class="table table-bordered table-hover data-tables" data-options='{ "paging": false; "searching":false}'>
									<thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal Pelayanan</th>
                                            <th>Usia Anak Terakhir</th>
                                            <th>KB Jenis</th>
                                            <th>KB Lama</th>
                                            <th>Aksi</th>
                                        </tr>
									</thead>
									<tbody>
                                        <?php $no=1; foreach($konsultasi->result() as $row) { ?> 
                                            <tr>
                                                <td><?= $no++ ?></td>
                                                <td><?= $row->tgl_pelayanan ?></td>
                                                <td><?= $row->usia_anak_terakhir." th" ?></td>
                                                <td><?= $row->kb_jenis ?></td>
                                                <td><?= $row->kb_lama ?></td>
                                                <td>
                                                    <button class="btn btn-xs btn-warning" onclick="editKonsultasi('<?= $row->id_lay_puswus ?>')">Edit</button>
													<button class="btn btn-xs btn-danger" onclick="hapusKonsultasi('<?= $row->id_lay_puswus ?>')">Hapus</button>
                                                </td>
                                            </tr>    
                                        <?php } ?>
                                    </tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?php echo base_url('Ibu/TambahLayananIbuPUSWUS/'.$this->uri->segment(3))?>" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Konsultasi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
<!--						<div class="col-md-12 mb-3">-->
<!--							<label>Pilih Imunisasi</label>-->
<!--							<select name="imunisasi" class="form-control">-->
<!--								<option value="">-- Pilih Imunisasi --</option>-->
<!--								--><?php //foreach($imunisasi->result() as $row) { ?><!-- -->
<!--									<option value="--><?php //echo $row->id_imunisasi ?><!--">-->
<!--										--><?php //echo $row->nama_imunisasi." (stok ".$row->jumlah.")" ?>
<!--									</option>-->
<!--								--><?php //} ?>
<!--							</select>-->
<!--						</div>-->
						<div class="col-md-12 mb-3">
							<label>Tanggal Imunisasi TT</label>
							<input type="date" name="imuntt" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Usia Anak Terakhir</label>
							<input type="number" name="usia_anak" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>KB Jenis</label>
							<input type="text" name="kb_jenis" class="form-control" autocomplete="off" required>
                        </div>
                    </div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>KB Lama</label>
							<input type="text" name="kb_lama" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Keterangan</label>
							<textarea name="keterangan" rows="3" class="form-control"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="editKonsultasi" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" id="formEditKonsultasi">
				<div class="modal-header">
					<h5 class="modal-title">Edit Konsultasi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
<!--							<label>Pilih Imunisasi</label>-->
<!--							<select name="imunisasi" id="imunisasi_ed" class="form-control">-->
<!--								<option value="">-- Pilih Imunisasi --</option>-->
<!--								--><?php //foreach($imunisasi->result() as $row) { ?><!-- -->
<!--									<option value="--><?php //echo $row->id_imunisasi ?><!--">-->
<!--										--><?php //echo $row->nama_imunisasi." (stok ".$row->jumlah.")" ?>
<!--									</option>-->
<!--								--><?php //} ?>
<!--							</select>-->
								<label>Tanggal Imunisasi TT</label>
								<input type="date" name="imuntt" id="tt" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Usia Anak Terakhir</label>
							<input type="number" name="usia_anak" id="usia_anak_ed" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>KB Jenis</label>
							<input type="text" name="kb_jenis" id="kb_jenis_ed" class="form-control" autocomplete="off" required>
                        </div>
                    </div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>KB Lama</label>
							<input type="text" name="kb_lama" id="kb_lama_ed" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Keterangan</label>
							<textarea name="keterangan" id="keterangan_ed" rows="3" class="form-control"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Edit</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="hapusKonsultasi" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="formHapusKonsultasi" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Hapus Konsultasi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Apakah Anda yakin akan menghapus data tersebut?</p>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger btn-sm">Hapus</button>
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php echo $footer ?>

<script>
	function editKonsultasi(val) {
		$.ajax({
			url: "<?php echo site_url('Ibu/get_layanan_ibu_puswus/'.$this->uri->segment(3).'/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#editKonsultasi").modal("show");
				$("#formEditKonsultasi").attr("action", "<?php echo base_url('Ibu/EditLayananIbuPUSWUS/') ?>"+data.id_lay_puswus);
				// $("#imunisasi_ed").val(data.id_imunisasi);
				$("#tt").val(data.tgl_imunisasi_tt);
				$("#usia_anak_ed").val(data.usia_anak_terakhir);
				$("#kb_jenis_ed").val(data.kb_jenis);
				$("#kb_lama_ed").val(data.kb_lama);
				$("#keterangan_ed").text(data.keterangan);
			},

			error: function (jqXHR, textStatus, errorThrown) {
				alert('Gagal mengambil data');
			}
		});
	}

	
	function hapusKonsultasi(val) {
		$.ajax({
			url: "<?php echo site_url('Ibu/get_layanan_ibu_puswus/'.$this->uri->segment(3).'/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#hapusKonsultasi").modal("show");
				$("#formHapusKonsultasi").attr("action", "<?php echo base_url('Ibu/HapusLayananIbuPUSWUS/') ?>"+data.id_lay_puswus);
			},

			error: function (jqXHR, textStatus, errorThrown) {
				alert('Gagal mengambil data');
			}
		});
	}
</script>
