<?php echo $header ?>
<div class="page has-sidebar-left">
	<?php if ($this->session->flashdata('message')){ ?>
		<div class="alert alert-dismissible <?= $this->session->flashdata('stts') == true ? 'alert-success bg-success' : 'alert-danger bg-danger' ?> text-white border-0 fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<?= $this->session->flashdata('message') ?>
		</div>
	<?php } ?>
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-child_friendly"></i> Data Ibu Hamil
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-lg-12">
				<div class="card ">
					<div class="card-header white">
						<i class="icon-child_friendly blue-text"></i>
						<strong> Data Ibu Hamil </strong>
						<div class="float-right">
							<button class="btn btn-success btn-xs" data-toggle="modal" data-target="#exportlaporan">Export Laporan</button>
						</div>
					</div>
					<div class="card-body">
						<table id="example2" class="table table-bordered table-hover data-tables" data-options='{ "paging": false; "searching":false}'>
							<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">NIK</th>
								<th class="text-center">Nama Ibu</th>
								<th class="text-center">Nama Suami</th>
								<th class="text-center">Tempat, Tanggal Lahir</th>
								<th class="text-center">Aksi</th>
							</tr>
							</thead>
							<tbody>
							<?php $no=1; foreach($hamil->result() as $row) { ?>
								<tr>
									<td class="text-center"><?php echo $no ?></td>
									<td><?php echo $row->nik_ibu ?></td>
									<td><?php echo $row->nama_ibu ?></td>
									<td><?php echo $row->suami_ibu ?></td>
									<td><?php echo $row->ibu_tempat_lahir.", ".date("d/m/Y", strtotime($row->ibu_tgl_lahir)) ?></td>
									<td class="text-center">
										<a href="<?php echo base_url()."ibu/detail_hamil/".$row->id_ibu ?>" class="btn btn-primary btn-xs">Detail</a>
										<button onclick="editIbu('<?php echo $row->id_ibu ?>')" class="btn btn-warning btn-xs">Edit</button>
										<button onclick="hapusIbu('<?php echo $row->id_user ?>')" class="btn btn-danger btn-xs">Hapus</a></button>
										<button onclick="lahirIbu('<?php echo $row->id_ibu ?>')" class="btn btn-secondary btn-xs">Melahirkan</a></button>
									</td>
								</tr>
								<?php $no++; } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="EditIbu" action="" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Edit Data Ibu</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nama Ibu</label>
							<input type="text" name="nama_ibu" id="namaibu" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nama Suami</label>
							<input type="text" name="nama_suami" id="suami" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nomor Telepon</label>
							<input type="text" name="nomor_tlp" id="telpon" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Alamat Ibu</label>
							<input type="text" name="alamat" id="alamat" class="form-control" required>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="st_title">Hapus Data Ibu</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="ui search focus lbel25">
					<label>Apakah Anda yakin ingin menghapus Data Ibu yang dipilih?</label>
				</div>
			</div>
			<div class="modal-footer">
				<button class="sbtn btn-danger btn-sm" id="link-hapus">Hapus</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="exportlaporan" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="st_title">Export Laporan Ibu Hamil</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="<?= site_url('ibu/laporan_ibu') ?>" method="post">
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Bulan</label>
							<select name="bulan" class="form-control" required>
								<option value="1">Januari</option>
								<option value="2">Februari</option>
								<option value="3">Maret</option>
								<option value="4">April</option>
								<option value="5">Mei</option>
								<option value="6">Juni</option>
								<option value="7">Juli</option>
								<option value="8">Agustus</option>
								<option value="9">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
							</select>
						</div>
						<div class="col-md-6 mb-3">
							<label>Tahun</label>
							<select name="tahun" class="form-control">
								<?php $year = date("Y"); for($i=$year; $i>=2000; $i--) { ?> 
									<option value="<?= $i ?>"><?= $i ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-success btn-sm" type="submit">Export</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="tambahAnak" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?php echo site_url('anak/LahiranAnak') ?>" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Tambah Anak</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
						<input type="hidden" id="idibu" name="id_ibu" class="form-control" required>
						<div class="col-md-12 mb-3">
							<label>KMS Anak</label>
							<input type="text" name="kms_anak" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nama Anak</label>
							<input type="text" name="nama_anak" class="form-control" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Tanggal Lahir</label>
							<input type="date" name="tgl_lahir" class="form-control" required>
						</div>
					</div>
<!--					<div class="form-row">-->
<!--						<div class="col-md-12 mb-3">-->
<!--							<label>Umur (Minggu)</label>-->
<!--							<input type="number" min="0" name="umur_anak" class="form-control" required>-->
<!--						</div>-->
<!--					</div>-->
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Tempat Lahir</label>
							<input type="text" name="tempat_lahir" class="form-control" required>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12 mb-3">
									<label>Berat Badan Lahir (Kg)</label>
									<input type="number" step="0.1" min="0" name="bb_lahir" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-row">
								<div class="col-md-12 mb-3">
									<label>Tinggi Badan Lahir (Cm)</label>
									<input type="number" step="0.1" min="0" name="tb_lahir" class="form-control" required>
								</div>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Jenis Kelamin</label>
							<select name="jkel" class="form-control">
								<option value="Laki-Laki">Laki-Laki</option>
								<option value="Perempuan">Perempuan</option>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
	function editIbu(id){
		$('#EditIbu').attr("action","<?= base_url('ibu/EditIbuHamil/') ?>" + id);
		$.ajax({
			url: "<?= base_url('ibu/detail_ibu/') ?>" + id,
			method: "GET",
			dataType: "JSON",
			success: function(data){
				$("#namaibu").val(data.nama_ibu);
				$("#suami").val(data.suami_ibu);
				$("#telpon").val(data.ibu_telpon);
				$("#alamat").val(data.alamat_ibu);
			}
		});
		$('#editModal').modal('show');
	}

  function lahirIbu(id){
    $("#idibu").val(id);
    $('#tambahAnak').modal('show');
  }

	function hapusIbu(id){
		$('#link-hapus').click(function(){
			window.location.href = "<?= base_url('Ibu/HapusIbu/') ?>" + id;
		});
		$('#deleteModal').modal('show');
	}

</script>

<?php echo $footer ?>
