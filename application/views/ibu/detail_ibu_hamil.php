<?php echo $header ?>

<style>
	td.heading {
		font-weight: 400;
	}
</style>

<div class="page has-sidebar-left">
	<?php if ($this->session->flashdata('message')){ ?>
		<div class="alert alert-dismissible <?= $this->session->flashdata('stts') == true ? 'alert-success bg-success' : 'alert-danger bg-danger' ?> text-white border-0 fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<?= $this->session->flashdata('message') ?>
		</div>
	<?php } ?>
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-child_friendly "></i> Detail Ibu
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-md-12">
				<a href="<?php echo site_url('ibu/hamil/')?>" class="btn btn-primary btn-sm mb-3">Kembali</a>
			</div>
			<div class="col-lg-3">
				<div class="card">
					<div class="card-body text-center">
						<div class="image m-3">
							<img class="user_avatar no-b no-p r-5" src="<?php echo base_url() ?>assets/img/ibu.png">
						</div>
						<div>
							<h6 class="p-t-10"><?php echo $ibu->nama_ibu ?></h6>
							<?php echo $ibu->nik_ibu ?>
						</div>
					</div>
				</div>
				<a href="<?php echo site_url('ibu/edit_detail_ibu/'.$this->uri->segment(3)) ?>" class="btn btn-sm btn-primary mt-3 btn-block">Edit Data Detail</a>
			</div>
			<div class="col-lg-9">
				<div class="align-self-center">
					<ul class="nav nav-pills mb-3" role="tablist">
						<li class="nav-item">
							<a class="nav-link active show" data-toggle="tab" href="#tabInformasi" role="tab" aria-controls="tab1" aria-expanded="true" aria-selected="true">Informasi</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="tab" href="#tabImunisasi" role="tab" aria-controls="tab2" aria-selected="false">Histori Konsultasi</a>
						</li>
					</ul>
				</div>
				<div class="card">
					<div class="card-body">
						<div class="tab-content">
							<div class="tab-pane fade show active" id="tabInformasi" role="tabpanel" aria-labelledby="tabInformasi">
								<table class="table table-bordered table-hover">
									<tr>
										<td class="heading" width="200">Alamat Ibu</td>
										<td><?php echo $ibu->alamat_ibu ?></td>
									</tr>
                                    <tr>
                                        <td class="heading">Nama Suami</td>
                                        <td><?php echo $ibu->suami_ibu ?></td>
                                    </tr>
                                    <tr>
                                        <td class="heading">Tanggal Lahir</td>
                                        <td><?php echo date("d/m/Y", strtotime($ibu->ibu_tgl_lahir)) ?></td>
                                    </tr>
                                    <tr>
                                        <td class="heading">Umur</td>
                                        <td><?php echo $ibu->ibu_umur ?></td>
                                    </tr>
                                    <tr>
                                        <td class="heading">No Telepon</td>
                                        <td><?php echo $ibu->ibu_telpon ?></td>
                                    </tr>
                                    <tr>
                                        <td class="heading">Status</td>
                                        <td>
                                            <?php
                                                if($ibu->status_ibu == 1) {
                                                    echo "Hamil";
                                                } else if($ibu->status_ibu == 2) {
                                                    echo "PUS / WUS";
                                                }
                                            ?>
                                        </td>
                                    </tr>
									<tr>
										<td class="heading">Tanggal Kehamilan</td>
										<td><?php echo $ibu->tanggal_kehamilan; ?></td>
									</tr>
									<tr>
										<td class="heading">Umur Kehamilan (Minggu)</td>
										<td><?php echo calWeek($ibu->tanggal_kehamilan) ?></td>
									</tr>
								</table>
								<table class="table table-bordered table-hover">
									<tr>
										<td class="heading">Berat Badan</td>
										<td colspan="3"><?php echo $ibu->berat_badan." kg" ?></td>
									</tr>
									<tr>
										<td class="heading">Penggunaan Kontrasepsi Sebelum Kehamilan Ini</td>
										<td colspan="3"><?php echo $ibu->penggunaan_kontrasepsi ?></td>
									</tr>
									<tr>
										<td class="heading">Jumlah Persalinan</td>
										<td><?php echo $ibu->jml_persalinan ?></td>
										<td width="200" class="heading">Jumlah Keguguran</td>
										<td><?php echo $ibu->jml_keguguran ?></td>
									</tr>
									<tr>
										<td class="heading">Jumlah Anak Hidup</td>
										<td><?php echo $ibu->jml_anak_hidup ?></td>
										<td class="heading">Jumlah Lahir Mati</td>
										<td><?php echo $ibu->jml_lahir_mati ?></td>
									</tr>
<!--									<tr>-->
<!--										<td class="heading">Jumlah Anak Lahir Kurang Bulan</td>-->
<!--										<td colspan="3">--><?php //echo $ibu->jml_anak_lahir_kurang_bulan ?><!--</td>-->
<!--									</tr>-->
<!--									<tr>-->
<!--										<td class="heading">Jarak Kehamilan ini dengan Persalinan Terakhir</td>-->
<!--										<td colspan="3">--><?php //echo $ibu->jarak_kehamilan ?><!--</td>-->
<!--									</tr>-->
									<tr>
										<td class="heading">Status Imunisasi TT</td>
										<td><?php echo $ibu->status_imun_tt ?></td>
										<td class="heading">Imunisasi TT Terakhir</td>
										<td><?php echo date("d/m/Y", strtotime($ibu->imun_tt_terakhir)) ?></td>
									</tr>
									<tr>
										<td class="heading">Penolong Persalinan Terakhir</td>
										<td colspan="3"><?php echo $ibu->penolong_persalinan ?></td>
									</tr>
									<tr>
										<td class="heading">Cara Persalinan Terakhir</td>
										<td colspan="3"><?php echo $ibu->cara_persalinan ?></td>
									</tr>
								</table>
							</div>
							<div class="tab-pane fade" id="tabImunisasi" role="tabpanel" aria-labelledby="tabImunisasi">
								<button class="btn btn-primary btn-sm mb-3" data-toggle="modal" data-target="#exampleModal">Tambah Data</button>
								<table id="example2" class="table table-bordered table-hover data-tables" data-options='{ "paging": false; "searching":false}'>
									<thead>
									<tr>
										<th>No</th>
										<th>Tanggal Pelayanan</th>
										<th>Keluhan</th>
										<th>Umur Kehamilan</th>
										<th width="100">Aksi</th>
									</tr>
									</thead>
									<tbody>
										<?php $no=1; foreach($konsultasi->result() as $row) { ?>
											<tr>
												<td><?php echo $no ?></td>
												<td><?php echo date("d/m/Y", strtotime($row->tgl_pelayanan)) ?></td>
												<td><?php echo $row->keluhan ?></td>
												<td><?php echo calWeek($row->tanggal_kehamilan)." minggu" ?></td>
												<td>
													<button class="btn btn-xs btn-warning" onclick="editKonsultasi('<?php echo $row->id_lay_bumil ?>')">Edit</button>
													<button class="btn btn-xs btn-danger" onclick="hapusKonsultasi('<?php echo $row->id_lay_bumil ?>')">Hapus</button>
												</td>
											</tr>
										<?php $no++; } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form action="<?php echo base_url('Ibu/TambahLayananIbu/'.$this->uri->segment(3))?>" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Konsultasi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Pilih Imunisasi</label>
							<select name="imunisasi" class="form-control">
								<option value="">-- Pilih Imunisasi --</option>
								<?php foreach($imunisasi->result() as $row) { ?>
									<option value="<?php echo $row->id_imunisasi ?>">
										<?php echo $row->nama_imunisasi." (stok ".$row->jumlah.")" ?>
									</option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Keluhan Sekarang</label>
							<input type="text" name="keluhan" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Tekanan Darah (mm/Hg)</label>
							<input type="text" name="tek_darah" class="form-control" autocomplete="off" required>
						</div>
						<div class="col-md-6 mb-3">
							<label>Berat Badan (Kg)</label>
							<input type="number" name="bb" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Hamil ke</label>
							<input type="text" name="hamil_ke" class="form-control" autocomplete="off" required>
						</div>
						<div class="col-md-6 mb-3">
							<label>Umur Kehamilan (Minggu)</label>
							<input type="number" name="umur" class="form-control" autocomplete="off" readonly required value="<?php echo calWeek($ibu->tanggal_kehamilan) ?>">
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Lingkar Lengan</label>
							<input type="text" name="lingkar_lengan" class="form-control" autocomplete="off" required>
						</div>
<!--					</div>-->
<!--					<div class="form-row">-->
<!--						<div class="col-md-6 mb-3">-->
<!--							<label>Tinggi Fundus (cm)</label>-->
<!--							<input type="number" name="tinggi_fundus" class="form-control" autocomplete="off" required>-->
<!--						</div>-->
<!--					<div class="form-row">-->
						<div class="col-md-6 mb-3">
							<label>Letak Janin</label>
							<input type="text" name="letak_janin" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Detak Jantung Janin</label>
							<input type="number" name="detak_jantung" class="form-control" autocomplete="off" required>
						</div>
						<div class="col-md-6 mb-3">
							<label>Kaki Bengkak</label>
							<select name="kaki_bengkak" class="form-control">
								<option value="1">Ya</option>
								<option value="0">Tidak</option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Tempat Periksa Hamil</label>
							<input type="text" name="tempat_periksa" class="form-control" autocomplete="off">
						</div>
						<div class="col-md-6 mb-3">
							<label>Jenis Resiko Tinggi (Bila Ada)</label>
							<input type="text" name="resiko" class="form-control" autocomplete="off">
						</div>
					</div>
<!--							<label>Usia Anak Terakhir</label>-->
<!--							<input type="text" name="usia_anak" class="form-control" autocomplete="off">-->
<!--						</div>-->
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Punya BPJS / KIS</label>
							<select name="bpjs_kis" class="form-control">
								<option value="ya">Ya</option>
								<option value="tidak">Tidak</option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Hari Pertama Haid Terakhir</label>
							<input type="date" name="hpht" class="form-control" autocomplete="off" required>
						</div>
						<div class="col-md-6 mb-3">
							<label>Hari Taksiran Persalinan</label>
							<input type="date" name="htp" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Hasil Pemeriksaan Laboratorium</label>
							<textarea name="hasil_lab" rows="3" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Tindakan</label>
							<input type="text" name="tindakan" class="form-control" autocomplete="off">
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nasihat</label>
							<textarea name="nasihat" rows="3" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Keterangan</label>
							<textarea name="keterangan" rows="3" class="form-control"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="editKonsultasi" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form method="post" id="formEditKonsultasi">
				<div class="modal-header">
					<h5 class="modal-title">Edit Konsultasi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Pilih Imunisasi</label>
							<select name="imunisasi" id="imunisasi_ed" class="form-control">
								<option value="">-- Pilih Imunisasi --</option>
								<?php foreach($imunisasi->result() as $row) { ?>
									<option value="<?php echo $row->id_imunisasi ?>">
										<?php echo $row->nama_imunisasi." (stok ".$row->jumlah.")" ?>
									</option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Keluhan Sekarang</label>
							<input type="text" name="keluhan" id="keluhan_ed" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Tekanan Darah (mm/Hg)</label>
							<input type="text" name="tek_darah" id="tek_darah_ed" class="form-control" autocomplete="off" required>
						</div>
						<div class="col-md-6 mb-3">
							<label>Berat Badan (Kg)</label>
							<input type="number" name="bb" id="bb_ed" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Hamil ke</label>
							<input type="text" name="hamil_ke" id="hamil_ke_ed" class="form-control" autocomplete="off" required>
						</div>
<!--						<div class="col-md-6 mb-3">-->
<!--							<label>Umur Kehamilan</label>-->
<!--							<input type="number" name="umur" id="umur_ed" class="form-control" autocomplete="off" required>-->
<!--						</div>-->
<!--					<div class="form-row">-->
						<div class="col-md-6 mb-3">
							<label>Lingkar Lengan</label>
							<input type="text" name="lingkar_lengan" id="lingkar_lengan_ed" class="form-control" autocomplete="off" required>
						</div>
<!--					</div>-->
<!--					<div class="form-row">-->
<!--						<div class="col-md-6 mb-3">-->
<!--							<label>Tinggi Fundus (cm)</label>-->
<!--							<input type="number" name="tinggi_fundus" id="tinggi_fundus_ed" class="form-control" autocomplete="off" required>-->
<!--						</div>-->
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Detak Jantung Janin</label>
							<input type="number" name="detak_jantung" id="detak_jantung_ed" class="form-control" autocomplete="off" required>
						</div>
						<div class="col-md-6 mb-3">
							<label>Kaki Bengkak</label>
							<select name="kaki_bengkak" id="kaki_bengkak_ed" class="form-control">
								<option value="1">Ya</option>
								<option value="0">Tidak</option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Tempat Periksa Hamil</label>
							<input type="text" name="tempat_periksa" id="tempat_periksa_ed" class="form-control" autocomplete="off" required>
						</div>
						<div class="col-md-6 mb-3">
							<label>Jenis Resiko Tinggi (Bila Ada)</label>
							<input type="text" name="resiko" id="resiko_ed" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
<!--						<div class="col-md-6 mb-3">-->
<!--							<label>Usia Anak Terakhir</label>-->
<!--							<input type="text" name="usia_anak" id="usia_anak_ed" class="form-control" autocomplete="off" required>-->
<!--						</div>-->
						<div class="col-md-6 mb-3">
							<label>Letak Janin</label>
							<input type="text" name="letak_janin" id="letak_janin_ed" class="form-control" autocomplete="off" required>
						</div>
						<div class="col-md-6 mb-3">
							<label>Punya BPJS / KIS</label>
							<select name="bpjs_kis" id="bpjs_kis_ed" class="form-control">
								<option value="ya">Ya</option>
								<option value="tidak">Tidak</option>
							</select>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-6 mb-3">
							<label>Hari Pertama Haid Terakhir</label>
							<input type="date" name="hpht" id="hpht_ed" class="form-control" autocomplete="off" required>
						</div>
						<div class="col-md-6 mb-3">
							<label>Hari Taksiran Persalinan</label>
							<input type="date" name="htp" id="htp_ed" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Hasil Pemeriksaan Laboratorium</label>
							<textarea name="hasil_lab" id="hasil_lab_ed" rows="3" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Tindakan</label>
							<input type="text" name="tindakan" id="tindakan_ed" class="form-control" autocomplete="off" required>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Nasihat</label>
							<textarea name="nasihat" id="nasihat_ed" rows="3" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-row">
						<div class="col-md-12 mb-3">
							<label>Keterangan</label>
							<textarea name="keterangan" id="keterangan_ed" rows="3" class="form-control"></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
					<button type="submit" class="btn btn-primary btn-sm">Edit</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="hapusKonsultasi" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="formHapusKonsultasi" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Hapus Konsultasi</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Apakah Anda yakin akan menghapus data tersebut?</p>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger btn-sm">Hapus</button>
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Batal</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php echo $footer ?>

<script>
	function editKonsultasi(val) {
		$.ajax({
			url: "<?php echo site_url('Ibu/get_layanan_ibu/'.$this->uri->segment(3).'/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#editKonsultasi").modal("show");
				$("#formEditKonsultasi").attr("action", "<?php echo base_url('Ibu/EditLayananIbu/') ?>"+data.id_lay_bumil);
				$("#imunisasi_ed").val(data.id_imunisasi);
				$("#keluhan_ed").val(data.keluhan);
				$("#bb_ed").val(data.berat_badan);
				$("#tek_darah_ed").val(data.tekanan_darah);
				$("#hamil_ke_ed").val(data.hamil_ke);
				$("#umur_ed").val(data.umur_kehamilan);
				$("#lingkar_lengan_ed").val(data.lingkar_lengan);
				$("#tinggi_fundus_ed").val(data.tinggi_fundus);
				$("#letak_janin_ed").val(data.letak_janin);
				$("#detak_jantung_ed").val(data.denyut_jantung_janin);
				$("#kaki_bengkak_ed").val(data.kaki_bengkak);
				$("#tempat_periksa_ed").val(data.tempat_periksa_hamil);
				$("#resiko_ed").val(data.jenis_resiko_tinggi);
				$("#usia_anak_ed").val(data.usia_anak_terakhir);
				$("#bpjs_kis_ed").val(data.bpjs_kis);
				$("#hpht_ed").val(data.tanggal_hpht);
				$("#htp_ed").val(data.tanggal_htp);
				$("#tindakan_ed").val(data.tindakan);
				$("#nasihat_ed").text(data.nasihat);
				$("#keterangan_ed").text(data.keterangan);
			},

			error: function (jqXHR, textStatus, errorThrown) {
				alert('Gagal mengambil data');
			}
		});
	}


	function hapusKonsultasi(val) {
		$.ajax({
			url: "<?php echo site_url('Ibu/get_layanan_ibu/'.$this->uri->segment(3).'/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#hapusKonsultasi").modal("show");
				$("#formHapusKonsultasi").attr("action", "<?php echo base_url('Ibu/HapusLayananIbu/') ?>"+data.id_lay_bumil);
			},

			error: function (jqXHR, textStatus, errorThrown) {
				alert('Gagal mengambil data');
			}
		});
	}
</script>
