<?php echo $header ?>

<div class="page has-sidebar-left">
	<?php if ($this->session->flashdata('message')) { ?>
		<div
			class="alert alert-dismissible <?= $this->session->flashdata('stts') == true ? 'alert-success bg-success' : 'alert-danger bg-danger' ?> text-white border-0 fade show"
			role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
					aria-hidden="true">&times;</span></button>
			<?= $this->session->flashdata('message') ?>
		</div>
	<?php } ?>
	<header class="my-3">
		<div class="container-fluid">
			<div class="row">
				<div class="col">
					<h1 class="s-24">
						<i class="icon-child_friendly"></i> Edit Detail Ibu
					</h1>
				</div>
			</div>
		</div>
	</header>
	<div class="container-fluid my-3">
		<div class="row">
			<div class="col-md-12">
				<a href="<?php echo site_url('ibu/detail_hamil/' . $this->uri->segment(3)) ?>"
					 class="btn btn-primary btn-sm mb-3">Kembali</a>
			</div>
			<div class="col-lg-3">
				<div class="card">
					<div class="card-body text-center">
						<div class="image m-3">
							<img class="user_avatar no-b no-p r-5" src="<?php echo base_url() ?>assets/img/ibu.png">
						</div>
						<div>
							<h6 class="p-t-10"><?php echo $ibu->nama_ibu ?></h6>
							<?php echo $ibu->nik_ibu ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-9">
				<div class="card">
					<form action="<?php echo site_url('ibu/EditDetailIbu/' . $this->uri->segment(3)) ?>" method="post">
						<div class="card-body">
							<div class="form-row">
								<div class="col-md-6 mb-3">
									<label>Berat Badan (kg)</label>
									<input type="number" name="berat_badan" class="form-control" autocomplete="off"
												 value="<?php echo $ibu->berat_badan ?>" required>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-6 mb-3">
									<label>Umur</label>
									<input type="number" name="umur" class="form-control" autocomplete="off"
												 value="<?php echo $ibu->ibu_umur ?>" required>
								</div>
								<div class="col-md-6 mb-3">
									<label>Tanggal Lahir</label>
									<input type="date" name="tgl_lahir" class="form-control" autocomplete="off"
												 value="<?php echo $ibu->ibu_tgl_lahir ?>" required>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-12 mb-3">
									<label>Penggunaan Kontrasepsi Sebelum Kehamilan Ini</label>
									<textarea name="kontrasepsi" id="" cols="30" rows="3"
														class="form-control"><?php echo $ibu->penggunaan_kontrasepsi ?></textarea>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-6 mb-3">
									<label>Jumlah Persalinan</label>
									<input type="number" name="persalinan" class="form-control" autocomplete="off"
												 value="<?php echo $ibu->jml_persalinan ?>" required>
								</div>
								<div class="col-md-6 mb-3">
									<label>Jumlah Keguguran</label>
									<input type="number" name="keguguran" class="form-control" autocomplete="off"
												 value="<?php echo $ibu->jml_keguguran ?>" required>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-6 mb-3">
									<label>Jumlah Anak Hidup</label>
									<input type="number" name="anak_hidup" class="form-control" autocomplete="off"
												 value="<?php echo $ibu->jml_anak_hidup ?>" required>
								</div>
								<div class="col-md-6 mb-3">
									<label>Jumlah Lahir Mati</label>
									<input type="number" name="lahir_mati" class="form-control" autocomplete="off"
												 value="<?php echo $ibu->jml_lahir_mati ?>" required>
								</div>
							</div>
<!--							<div class="form-row">-->
<!--								<div class="col-md-6 mb-3">-->
<!--									<label>Jumlah Anak Lahir Kurang Bulan</label>-->
<!--									<input type="number" name="kurang_bulan" class="form-control" autocomplete="off"-->
<!--												 value="--><?php //echo $ibu->jml_anak_lahir_kurang_bulan ?><!--" required>-->
<!--								</div>-->
<!--								<div class="col-md-6 mb-3">-->
<!--									<label>Jarak Kehamilan ini dengan Persalinan Terakhir</label>-->
<!--									<input type="number" name="persalinan_terakhir" class="form-control" autocomplete="off"-->
<!--												 value="--><?php //echo $ibu->jarak_kehamilan ?><!--" required>-->
<!--								</div>-->
<!--							</div>-->
							<div class="form-row">
								<div class="col-md-6 mb-3">
									<label>Status Imunisasi TT</label>
									<input type="text" name="stts_imunisasi" class="form-control" autocomplete="off"
												 value="<?php echo $ibu->status_imun_tt ?>" required>
								</div>
								<div class="col-md-6 mb-3">
									<label>Imunisasi TT Terakhir</label>
									<input type="date" name="imunisasi_terakhir" class="form-control" autocomplete="off"
												 value="<?php echo $ibu->imun_tt_terakhir ?>" required>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-12 mb-3">
									<label>Penolong Persalinan Terakhir</label>
									<input type="text" name="penolong_salin" class="form-control" autocomplete="off"
												 value="<?php echo $ibu->penolong_persalinan ?>" required>
								</div>
							</div>
							<div class="form-row">
								<div class="col-md-12 mb-3">
									<label>Cara Persalinan Terakhir</label>
									<input type="text" name="cara_salin" class="form-control" autocomplete="off"
												 value="<?php echo $ibu->cara_persalinan ?>" required>
								</div>
							</div>
						</div>
						<div class="card-footer">
							<button type="submit" class="btn btn-primary">Edit</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $footer ?>

