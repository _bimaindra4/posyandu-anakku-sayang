<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Calculate week between date and now
 *
 * @param date $tanggalHamil
 * @return int $week
 */
function calWeek($tanggalHamil)
{
	date_default_timezone_set('Asia/Jakarta');
	$sekarang = date('Y-m-d');
	$sekarang = DateTime::createFromFormat('Y-m-d', $sekarang);
	$tanggalHamil = DateTime::createFromFormat('Y-m-d', $tanggalHamil);
	return floor($sekarang->diff($tanggalHamil)->days/7);
}
